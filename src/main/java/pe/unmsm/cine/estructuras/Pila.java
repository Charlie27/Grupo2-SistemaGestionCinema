/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.estructuras;

/**
 *
 * @author charlie
 * @param <T>
 */
public class Pila<T>{
    
    private Nodo<T> cabeza;
    
    private class Nodo<T>{
        T dato;
        Nodo<T> sgte;
        
        Nodo(T dato){
            this.dato = dato;
            sgte = null;
        }
    }
    
    public void push(T dato){
        Nodo<T> nuevo = new Nodo<>(dato);
        if(cabeza == null){
            cabeza = nuevo;
        }else{
            nuevo.sgte = cabeza;
            cabeza = nuevo;
        }
    }
    
    public T pop(){
        if(cabeza == null) {
            return null;
        }
        T dato = cabeza.dato;
        cabeza = cabeza.sgte;
        return dato;
    }
    
    public boolean isEmpty(){
        return cabeza == null;
    }
}
