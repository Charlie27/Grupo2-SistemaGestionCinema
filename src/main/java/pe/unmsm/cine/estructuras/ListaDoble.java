package pe.unmsm.cine.estructuras;

import java.io.Serializable;

public class ListaDoble<T> implements Serializable, ToArray{

    private NodoDoble<T> cabecera;
    private NodoDoble<T> ultimo;
    int tam = 0;

    private class NodoDoble<T> implements Serializable{

        T dato;
        NodoDoble<T> sig, ant;

        NodoDoble(T dato) {
            this.dato = dato;
            sig = null;
            ant = null;
        }

        public T getDato() {
            return dato;
        }

        public void setDato(T dato) {
            this.dato = dato;
        }

        public NodoDoble<T> getSig() {
            return sig;
        }

        public void setSig(NodoDoble<T> sig) {
            this.sig = sig;
        }

        public NodoDoble<T> getAnt() {
            return ant;
        }

        public void setAnt(NodoDoble<T> ant) {
            this.ant = ant;
        }
    }

    public NodoDoble<T> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoDoble<T> cabecera) {
        this.cabecera = cabecera;
    }

    public void insertarAlInicio(T nuevo) {
        NodoDoble<T> nuevoNodo = new NodoDoble<>(nuevo);
        nuevoNodo.sig = cabecera;

        if (cabecera != null) {
            cabecera.ant = nuevoNodo;
        } else {
            ultimo = nuevoNodo;
        }
        cabecera = nuevoNodo;
        tam++;
    }

    public void insertarAlFinal(T nuevo) {
        NodoDoble<T> nuevoNodo = new NodoDoble<>(nuevo);
        nuevoNodo.ant = ultimo;

        if (cabecera != null) {
            ultimo.sig = nuevoNodo;
        } else {
            cabecera = nuevoNodo;
        }
        ultimo = nuevoNodo;
        tam++;
    }

    @Override
    public String toString() {
        String cadena = "";
        NodoDoble<T> aux = cabecera;
        while (aux != null) {
            cadena += aux.dato + ", ";
            aux = aux.sig;
        }
        return cadena;
    }

    private void intercambiar(NodoDoble<T> i, NodoDoble<T> j) {

        T aux = i.dato;
        i.dato = j.dato;
        j.dato = aux;
    }

    public int getTam() {
        return tam;
    }

    public T get(int pos) {
        if (cabecera != null && pos >= 0 && pos < tam) {

            NodoDoble<T> aux = cabecera;
            int cont = 0;
            while (cont != pos && aux != null) {
                cont++;
                aux = aux.sig;
            }
            return aux.dato;

        } else {
            System.out.println("ERROR en metodo get de ListaDoble. Cabecera es null, o la posicion a eliminar esta fuera de rango");
            return null;
        }
    }

    public void eliminar(int pos) {//pos inicia de 0

        if (cabecera != null && pos >= 0 && pos < tam) {
            
            if (pos == 0) {                     //eliminar primer elemento
                if (tam != 1) {                 //si tiene mas de un elemento
                    cabecera = cabecera.sig;
                    cabecera.ant.sig = null;
                    cabecera.ant = null;
                } else {                        //si tiene 1 elemento
                    cabecera = null;
                }
            } else if (pos == tam - 1) {        //si se quiere eliminar el ultimo elemento
                ultimo = ultimo.ant;
                ultimo.sig.ant = null;
                ultimo.sig = null;
            } else {                            //eliminar elemento intermedio

                NodoDoble<T> aux = cabecera;
                int cont = 0;
                while (cont != pos && aux != null) {
                    cont++;
                    aux = aux.sig;
                }

                aux.ant.sig = aux.sig;
                aux.sig.ant = aux.ant;
                aux.ant = null;
                aux.sig = null;

            }
            tam--;
        } else {
            System.out.println("Cabecera es null, o la posicion a eliminar esta fuera de rango");
        }
    }
    
    public Object[] toArray(){
        Object[] array = new Object[tam];
        NodoDoble<T> nodo = cabecera;
        int i = 0;
        while(nodo != null){
            array[i] = nodo.getDato();
            nodo = nodo.getSig();
            i++;
        }
        return array;
    }
    
    public int size(){
        return tam;
    }
}
