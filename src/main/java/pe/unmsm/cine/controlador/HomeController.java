/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import pe.unmsm.cine.dao.CinesDAO;
import pe.unmsm.cine.dao.CiudadDAO;
import pe.unmsm.cine.dao.DulcesDAO;
import pe.unmsm.cine.dao.PeliculasDAO;
import pe.unmsm.cine.dao.PerfilesDAO;
import pe.unmsm.cine.entidades.Cine;
import pe.unmsm.cine.entidades.Ciudad;
import pe.unmsm.cine.entidades.Dulce;
import pe.unmsm.cine.entidades.Pelicula;
import pe.unmsm.cine.entidades.Perfil;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author charlie
 */
@WebServlet(name = "HomeController", urlPatterns = {"/Home"})
public class HomeController extends HttpServlet {
    private CinesDAO cinesDAO;
    private DulcesDAO dulcesDAO;
    private CiudadDAO ciudadDAO;
    private PeliculasDAO peliculasDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Access-Control-Allow-Origin","*");
        response.setHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
        response.setHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Empieza");
        long empieza = System.currentTimeMillis();
        System.out.println(empieza);
        processRequest(request, response);
        
        cinesDAO = new CinesDAO();
        dulcesDAO = new DulcesDAO();
        peliculasDAO = new PeliculasDAO();
        ciudadDAO = new CiudadDAO();
        
        ListaDoble<Cine> cines = cinesDAO.getAll();
        ListaDoble<Dulce> dulces = dulcesDAO.getAll();
        ListaDoble<Pelicula> peliculas = peliculasDAO.getAll();
        ListaDoble<Ciudad> ciudades = ciudadDAO.getAll();
        
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        for(int i = 0; i<cines.getTam(); i++){
            array.put(i, cines.get(i).getJson());
        }
        json.put("cines", array);
        
        array = new JSONArray();
        for(int i = 0; i<dulces.getTam(); i++){
            array.put(i, dulces.get(i).getJson());
        }
        json.put("dulces", array);
        
        array = new JSONArray();
        for(int i = 0; i<peliculas.getTam(); i++){
            array.put(i, peliculas.get(i).getJson());
        }
        json.put("peliculas", array);
        
        array = new JSONArray();
        for(int i = 0; i<ciudades.getTam(); i++){
            array.put(i, ciudades.get(i).getJson());
        }
        json.put("ciudades", array);
        
        response.setContentType("application/json");
        System.out.println("Termina");
        long termina = System.currentTimeMillis();
        System.out.println(termina);
        System.out.println("Tiempo de espera: " + (termina - empieza));
        try (PrintWriter out = response.getWriter()) {
            out.write(json.toString());
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
