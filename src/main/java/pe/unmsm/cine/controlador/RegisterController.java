/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;
import pe.unmsm.cine.dao.UsuarioDAO;
import pe.unmsm.cine.entidades.Usuario;
import pe.unmsm.cine.estructuras.Pila;

/**
 *
 * @author charlie
 */
@WebServlet(name = "RegisterController", urlPatterns = {"/Register"})
public class RegisterController extends HttpServlet {
    
    UsuarioDAO usuarioDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Access-Control-Allow-Origin","*");
        response.setHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
        response.setHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /*@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String name = request.getParameter("name");
        String apePat = request.getParameter("apePat");
        String apeMat = request.getParameter("apeMat");
        String dni = request.getParameter("dni");
        String password = request.getParameter("password");
        Pila<String> pilaRequerimientos = new Pila<>();
        pilaRequerimientos.push(name);
        pilaRequerimientos.push(apePat);
        pilaRequerimientos.push(apeMat);
        pilaRequerimientos.push(dni);
        pilaRequerimientos.push(password);
        
        JSONObject json = new JSONObject();
        
        String res = isVoid(pilaRequerimientos);
        
        if(res == null){
            usuarioDAO = new UsuarioDAO();
            password = BCrypt.hashpw(password, BCrypt.gensalt());
            if(usuarioDAO.registrar(new Usuario(name, apePat, apeMat, dni, password))){
                json = usuarioDAO.getByDni(dni).getJson();
            }else{
                res = "No se pudo registrar, intentelo de nuevo";
                json.put("err", res);
            }
        }else{
            json.put("err", res);
        }
        
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            out.write(json.toString());
        }
        
    }
    
    private String isVoid(Pila<String> pila){
        String res = null;
        while(!pila.isEmpty()){
            String parameter = pila.pop();
            if(parameter == null || parameter.equals("")){
                res = "Error, se han enviado campos vacios";
                break;
            }
        }
        return res;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
