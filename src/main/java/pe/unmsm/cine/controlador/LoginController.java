package pe.unmsm.cine.controlador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;
import pe.unmsm.cine.dao.EmpleadoDAO;
import pe.unmsm.cine.dao.UsuarioDAO;
import pe.unmsm.cine.entidades.Empleado;
import pe.unmsm.cine.entidades.Usuario;

/**
 *
 * @author charlie
 */
@WebServlet(urlPatterns = {"/Login"})
public class LoginController extends HttpServlet {
    private UsuarioDAO usuarioDAO;
    private EmpleadoDAO empleadoDAO;
    private BCrypt bCrypt;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Access-Control-Allow-Origin","*");
        response.setHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
        response.setHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /*@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String user = request.getParameter("user");
        String password = request.getParameter("password");
        JSONObject res = new JSONObject();
        if(user != null && password != null){
            
            usuarioDAO = new UsuarioDAO();
            Usuario usuario = usuarioDAO.getByDni(user);

            if(usuario != null){
                if(BCrypt.checkpw(password, usuario.getContrasenia())){
                    if(usuario.getPerfil().getIdPerfil() == 2 || usuario.getPerfil().getIdPerfil() == 1){
                        empleadoDAO = new EmpleadoDAO();
                        Empleado empleado = empleadoDAO.getByUserId(usuario.getCodigo());
                        res = empleado.getJson();
                    }else{
                        res = usuario.getJson();
                    }
                }else{
                    res.put("err", "La contraseña es incorrecta");
                }
            }else{
                res.put("err", "No existe el usuario");
            }
        }else{
            res.put("err", "falta enviar datos");
        }
        
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            out.write(res.toString());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
