/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class TipoBoleto {
    private int codigo;
    private String modelo;// // Depende de la edad
    private String descripcion;
    
    public TipoBoleto(int codigo, String modelo,String descripcion){
        this.modelo=modelo;
        this.descripcion=descripcion;
    }
    
    
    public TipoBoleto(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.modelo = json.getString("modelo");
        this.descripcion = json.getString("descripcion");
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("modelo", modelo);
        j.put("descripcion", descripcion);
        return j;
    }
    
    @Override
    public String toString() {
        return "TipoBoleto{" + "codigo=" + codigo + ", modelo=" + modelo + ", descripcion=" + descripcion + '}';
    }

   



    
}
