/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Perfil {
    private int idPerfil;
    private String perfil;
    private String descripcion;
    
    public Perfil(int idPerfil,String perfil, String descripcion){
        this.idPerfil=idPerfil;
        this.perfil=perfil;
        this.descripcion=descripcion;
    }
    
    
    public Perfil(JSONObject json) throws JSONException{
        this.idPerfil = json.getInt("codigo");
        this.perfil = json.getString("tipo");
        this.descripcion = json.getString("descripcion");
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int codigo) {
        this.idPerfil = codigo;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String tipo) {
        this.perfil = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("idPerfil", idPerfil);
        j.put("perfil", perfil);
        j.put("descripcion", descripcion);
        return j;
    }
    
    @Override
    public String toString() {
        return "Perfil{" + "codigo=" + idPerfil + ", tipo=" + perfil + ", descripcion=" + descripcion + '}';
    }
    
}
