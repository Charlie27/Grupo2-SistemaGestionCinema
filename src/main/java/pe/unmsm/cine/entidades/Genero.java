/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Genero {
    private int codigo;
    private String nombre;
    
    public Genero(int codigo, String nombre){
        this.codigo=codigo;
        this.nombre=nombre;
    }
    
    public Genero(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.nombre = json.getString("nombre");
    }
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("nombre", nombre);
        return j;
    }
    
    @Override
    public String toString() {
        return "Genero{" + "codigo=" + codigo + ", nombre=" + nombre + '}';
    }
   


}
    
