/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class VentaBoleto {
    private int codigo;
    private Venta venta;
    private Boleto boleto;
    
    public VentaBoleto(int codigo,Venta venta, Boleto boleto){
        this.codigo=codigo;
        this.venta=venta;
        this.boleto=boleto;
    }
    
    
    public VentaBoleto(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        //this.venta=new Venta(json.getJSONObject("venta"));
        this.boleto=new Boleto(json.getJSONObject("boleto"));
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }
    
    public Boleto getBoleto() {
        return boleto;
    }

    public void setBoleto(Boleto boleto) {
        this.boleto = boleto;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("venta", venta);
        j.put("boleto", boleto);
        return j;
    }
    
    @Override
    public String toString() {
        return "VentaBoleto{" + "codigo=" + codigo + ", venta=" + venta + ", boleto=" + boleto + '}';
    }

  
}
    
