
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class VentaPromocion {
    private int codigo;
    private Venta venta;
    private Promocion promocion;
    
    public VentaPromocion(int codigo,Venta venta, Promocion promocion){
        this.codigo=codigo;
        this.venta=venta;
        this.promocion=promocion;
    }
    
    
public VentaPromocion(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        //this.venta=new Venta(json.getJSONObject("venta"));
        this.promocion=new Promocion(json.getJSONObject("promocion"));
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }
    
    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("venta", venta);
        j.put("promocion", promocion);
        return j;
    }
    
    @Override
    public String toString() {
        return "VentaPromocion{" + "codigo=" + codigo + ", venta=" + venta + ", promocion=" + promocion + '}';
    }

  
}
    
