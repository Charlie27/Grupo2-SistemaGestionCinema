/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Estado {
    private int codigo;
    private String estado;
   
    public Estado(int codigo , String estado){
        this.codigo=codigo;
        this.estado=estado;
    }
    
    public Estado(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.estado = json.getString("estado");
    }
    
    public int getCodigo(){
        return codigo;
    }
    public void setCodigo(int codigo){
        this.codigo=codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("estado", estado);
        return j;
    }
    
    @Override
    public String toString() {
        return "Dia{" + "codigo=" + codigo + ", estado=" + estado + '}';
    }
    
}
