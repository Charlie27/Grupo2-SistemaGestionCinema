/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Dia {
    private int codigo;
    private String dia;
   
    public Dia(int codigo , String dia){
        this.codigo=codigo;
        this.dia=dia;
    }
    
    public Dia(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.dia = json.getString("dia");
    }
    
    public int getCodigo(){
        return codigo;
    }
    public void setCodigo(int codigo){
        this.codigo=codigo;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("dia", dia);
        return j;
    }
    
    @Override
    public String toString() {
        return "Dia{" + "codigo=" + codigo + ", dia=" + dia + '}';
    }
    
}
