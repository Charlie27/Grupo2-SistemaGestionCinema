/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class DulcePromocion {
    private int codigo;
    private Dulce dulce;
    private Promocion promocion;
   
public DulcePromocion(int codigo,Dulce dulce,Promocion promocion){
    this.codigo=codigo;
    this.dulce=dulce;
    this.promocion=promocion;
}

public DulcePromocion(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.dulce=new Dulce(json.getJSONObject("dulce"));
        this.promocion=new Promocion(json.getJSONObject("promocion"));
        
    }


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Dulce getDulce() {
        return dulce;
    }

    public void setDulce(Dulce dulce) {
        this.dulce = dulce;
    }
    

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("dulce", dulce);
        j.put("promocion",promocion);
        return j;
    }
    
    @Override
    public String toString() {
        return "DulcePromocion{" + "codigo=" + codigo + ", dulce=" + dulce + ", promocion=" + promocion + '}';
    }

}


