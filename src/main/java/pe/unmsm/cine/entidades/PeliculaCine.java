/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class PeliculaCine {
    private int codigo;
    private Pelicula pelicula;
    private Cine cine;
    
    public PeliculaCine(int codigo, Pelicula pelicula,Cine cine){
        this.codigo=codigo;
        this.pelicula=pelicula;
        this.cine=cine;
    } 
    
    
    public PeliculaCine(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.pelicula=new Pelicula(json.getJSONObject("pelicula"));
        this.cine=new Cine(json.getJSONObject("cine"));
        
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public Cine getCine() {
        return cine;
    }

    public void setCine(Cine cine) {
        this.cine = cine;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("pelicula", pelicula);
        j.put("cine", cine);
        return j;
    }
    
    @Override
    public String toString() {
        return "PeliculaActor{" + "codigo=" + codigo + ", pelicula=" + pelicula + ", cine=" + cine + '}';
    }
    
}
