/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Cliente {
    private int codigo;
    private String nombre;
    private String apellido;
    private int puntos;
    private int dni;
    
    
public Cliente(String nombre, String apellido, int puntos){
    this.nombre=nombre;
    this.apellido= apellido;
    this.puntos=puntos;
}


public Cliente(JSONObject json) throws JSONException{
    this.codigo = json.getInt("codigo");
    this.nombre = json.getString("nombre");
    this.apellido = json.getString("apellido");
    this.puntos = json.getInt("punto");
    this.dni = json.getInt("dni");
    
    }

 public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellido;
    }

    public void setApellidos(String apellido) {
        this.apellido = apellido;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
    
    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("nombre", nombre);
        j.put("apellido", apellido);
        j.put("puntos", puntos);
        j.put("dni", dni);
        return j;
    }
    
    
    @Override
    public String toString() {
        return "Cliente{" + "nombre=" + nombre + ", apellidos=" + apellido + ", puntos =" + puntos +  '}';
    }







}


