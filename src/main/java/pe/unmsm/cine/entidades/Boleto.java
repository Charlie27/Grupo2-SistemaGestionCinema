/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Boleto {
    private int codigo;
    private int precio;
    private int idTipoSala;
    private int idTipoBoleto;
    private int idDia;

    public Boleto(int codigo, int precio, int idTipoSala, int idTipoBoleto, int idDia) {
        this.codigo = codigo;
        this.precio = precio;
        this.idTipoSala = idTipoSala;
        this.idTipoBoleto = idTipoBoleto;
        this.idDia = idDia;
    }
    
    
    public Boleto(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.precio = json.getInt("precio");
        this.idTipoSala=json.getInt("sala");
        this.idTipoBoleto=json.getInt("boleto");
        this.idDia=json.getInt("dia");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getIdTipoSala() {
        return idTipoSala;
    }

    public void setIdTipoSala(int idTipoSala) {
        this.idTipoSala = idTipoSala;
    }

    public int getIdTipoBoleto() {
        return idTipoBoleto;
    }

    public void setIdTipoBoleto(int idTipoBoleto) {
        this.idTipoBoleto = idTipoBoleto;
    }

    public int getIdDia() {
        return idDia;
    }

    public void setIdDia(int idDia) {
        this.idDia = idDia;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("precio", precio);
        j.put("idTipoSala", idTipoSala);
        j.put("idTipoBoleto", idTipoBoleto);
        j.put("idDia", idDia);
        return j;
    }
    
    @Override
    public String toString() {
        return "Boleto{" + "codigo=" + codigo + ", precio=" + precio + ", sala=" +idTipoSala + ", boleto=" + idTipoBoleto + ", dia=" + idDia+ '}';
    }
    
}
