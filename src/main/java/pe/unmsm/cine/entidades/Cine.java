/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Cine {
    
    private int codigo;
    private String nombre; 
    private String direccion;
    private String img;
    private int salas;
    private int idCiudad;
    
    public Cine(int codigo, String nombre, String direccion, String img, int salas,int idCiudad){
        this.codigo = codigo;
        this.nombre=nombre;
        this.img=img;
        this.direccion=direccion;
        this.salas=salas;
        this.idCiudad=idCiudad;
    }
    
    public Cine(JSONObject json) throws JSONException{
        this.codigo=json.getInt("codigo");
        this.nombre=json.getString("nombre");
        this.direccion=json.getString("direccion");
        this.img=json.getString("img");
        this.salas=json.getInt("salas");
        this.idCiudad= json.getInt("ciudad");
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        this.idCiudad = idCiudad;
    }
    

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getSalas() {
        return salas;
    }

    public void setSalas(int salas) {
        this.salas = salas;
    }

    @Override
    public String toString() {
        return "Cine{" + "codigo=" + codigo + ", nombre=" + nombre + ", direccion=" + direccion + ", img=" + img + ", salas=" + salas + ", ciudad=" + idCiudad + '}';
    }
    
    
    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo",codigo);
        j.put("nombre",nombre);
        j.put("direccion",direccion);
        j.put("img",img);
        j.put("nmrSalas",salas);
        j.put("idCiudad",idCiudad);
        
        return j;
    }

    
}
