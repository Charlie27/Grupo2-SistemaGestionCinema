/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class PeliculaGenero {
    private int codigo;
    private Pelicula pelicula;
    private Genero genero;
    
    public PeliculaGenero(int codigo, Pelicula pelicula,Genero genero){
        this.codigo=codigo;
        this.pelicula=pelicula;
        this.genero=genero;
    }  
    
    
    public PeliculaGenero(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.pelicula=new Pelicula(json.getJSONObject("pelicula"));
        this.genero=new Genero(json.getJSONObject("genero"));
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("pelicula", pelicula);
        j.put("genero", genero);
        return j;
    }
    
    @Override
    public String toString() {
        return "PeliculaGenero{" + "codigo=" + codigo + ", pelicula=" + pelicula + ", genero=" + genero + '}';
    }
    
}
