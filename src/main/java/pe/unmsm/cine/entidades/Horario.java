/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import java.sql.Date;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Horario {
    
    private int codigo;
    private Cine cine;
    private Pelicula pelicula;
    private Sala sala;
    private int duracion;
    
    public Horario(int codigo, Cine cine, Pelicula pelicula, Sala sala, int duracion){
        this.pelicula=pelicula;
        this.cine=cine;
        this.sala=sala;
        this.duracion=duracion;        
    } 
    
    
    public Horario(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        
        this.cine=new Cine(json.getJSONObject("cine"));
        this.pelicula=new Pelicula(json.getJSONObject("pelicula"));
        this.sala=new Sala(json.getJSONObject("sala"));
        
        this.duracion = json.getInt("duracion");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public Cine getCine() {
        return cine;
    }

    public void setCine(Cine cine) {
        this.cine = cine;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("nombre", cine);
        j.put("direccion", pelicula);
        j.put("telefono", sala);
        j.put("ruc", duracion);
        return j;
    }
    
    @Override
    public String toString() {
        return "Horario{" + "codigo=" + codigo + ", cine=" + cine + ", pelicula=" + pelicula + ", sala=" + sala + ", duracion=" + duracion + '}';
    }

    
}
