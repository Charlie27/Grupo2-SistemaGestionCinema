
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class VentaDulce {
    private int codigo;
    private Venta venta;
    private Dulce dulce;
    
    public VentaDulce(int codigo,Venta venta, Dulce dulce){
        this.codigo=codigo;
        this.venta=venta;
        this.dulce=dulce;
    }
    
    
    public VentaDulce(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        //this.venta=new Venta(json.getJSONObject("venta"));
        this.dulce=new Dulce(json.getJSONObject("dulce"));
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }
    
    public Dulce getDulce() {
        return dulce;
    }

    public void setDulce(Dulce dulce) {
        this.dulce = dulce;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("venta", venta);
        j.put("dulce", dulce);
        return j;
    }
    
    @Override
    public String toString() {
        return "VentaDulce{" + "codigo=" + codigo + ", venta=" + venta + ", dulce=" + dulce + '}';
    }

  
}
    
