/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class PromocionBoleto {
    private int codigo;
    private Boleto boleto;
    private Promocion promocion;
    
    public PromocionBoleto(int codigo, Boleto boleto, Promocion promocion){
        this.codigo=codigo;
        this.boleto=boleto;
        this.promocion=promocion;
    }
    
    
    
public PromocionBoleto(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.boleto=new Boleto(json.getJSONObject("boleto"));
        this.promocion=new Promocion(json.getJSONObject("promocion"));
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Boleto getBoleto() {
        return boleto;
    }

    public void setBoleto(Boleto boleto) {
        this.boleto = boleto;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("boleto", boleto);
        j.put("promocion", promocion);
        return j;
    }
    
    @Override
    public String toString() {
        return "PromocionBoleto{" + "codigo=" + codigo + ", boleto=" + boleto + ", promocion=" + promocion + '}';
    }

  
}
    
