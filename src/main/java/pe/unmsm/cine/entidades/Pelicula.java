/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Pelicula {
    private int codigo;
    private String nombre;
    private int duracion;
    private String director;
    private String imagen;
    private String imagen_slider;
    private String trailer;
    private Idioma idioma; //Para elegir entre las opciones que ya estaran puestas.
    private Clasificacion clasificacion; //Para elegir entre las opciones de acuerdo a la edad.
    private Estado estado; //Para elegir si esta en semana de estreno  o no.

    public Pelicula(int codigo, String nombre, int duracion, String Director,String imagen, String imagen_slider,String trailer,Idioma idioma, Clasificacion clasificacion, Estado estado ){
        this.nombre=nombre;
        this.duracion=duracion;
        this.director=director;
        this.imagen=imagen;
        this.imagen_slider=imagen_slider;
        this.trailer=trailer;
        this.idioma=idioma;
        this.clasificacion=clasificacion;
        this.estado=estado;
    }
    
    public Pelicula(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.nombre = json.getString("nombre");
        this.duracion = json.getInt("duracion");
        this.director = json.getString("director");
        this.imagen = json.getString("imagen");
        this.imagen_slider = json.getString("imagen_slider");
        this.trailer = json.getString("trailder");
        this.clasificacion=new Clasificacion(json.getJSONObject("clasificacion"));
        this.estado=new Estado(json.getJSONObject("estado"));
    }
    

    public Pelicula(String nombre, int duracion, String director, String imagen, String imagen_slider, Idioma idioma, Clasificacion clasificacion, Estado estado) {
        this.nombre = nombre;
        this.duracion = duracion;
        this.director = director;
        this.imagen = imagen;
        this.imagen_slider = imagen_slider;
        this.idioma = idioma;
        this.clasificacion = clasificacion;
        this.estado = estado;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getImagen_slider() {
        return imagen_slider;
    }

    public void setImagen_slider(String imagen_slider) {
        this.imagen_slider = imagen_slider;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Idioma getIdioma() {
        return idioma;
    }

    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }
    
    public Clasificacion getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(Clasificacion clasificacion) {
        this.clasificacion = clasificacion;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("nombre", nombre);
        j.put("duracion", duracion);
        j.put("director", director);
        j.put("imagen", imagen);
        j.put("imagen_slider", imagen_slider);
        j.put("trailer", trailer);
        j.put("idioma", idioma);
        j.put("clasificacion", clasificacion);
        j.put("estado", estado);
        return j;
    }
    
    @Override
    public String toString() {
        return "Pelicula{" + "codigo=" + codigo + ", nombre=" + nombre + ", duracion=" + duracion + ", director=" + director + ", imagen=" + imagen + ", imagen_slider=" + imagen_slider + ", trailer=" + trailer + ", idioma=" + idioma + ", clasificacion=" + clasificacion + ", estado=" + estado + '}';
    }
    
    
}
