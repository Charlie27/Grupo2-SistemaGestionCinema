/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Venta {
    private int codigo;
    private double monto;
    private Date fecha; // no estoy seguro que libreria se usa para Date corregir cualquier cosa.
    private Usuario usuario;
    
    public Venta(int codigo, double monto /* ,Date fecha*/ ,Usuario usuario){
        this.codigo=codigo;
        this.monto=monto;
        this.fecha=fecha;
        this.usuario=usuario;
    }
    
    
    /*public Venta(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.monto = json.getDouble("monto");
        this.fecha= new Date(json.getString("fecha"));//Tiene date pero no se puede instanciar//
        this.usuario=new Usuario(json.getJSONObject("usuario"));
        
        
    }*/

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Usuario  getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("monto", monto);
        j.put("fecha", fecha);
        j.put("usuario", usuario);
        return j;
    }
    
    @Override
    public String toString() {
        return "Venta{" + "codigo=" + codigo + ", monto=" + monto + ", fecha=" + fecha + ", usuario=" + usuario + '}';
    }
    
}
