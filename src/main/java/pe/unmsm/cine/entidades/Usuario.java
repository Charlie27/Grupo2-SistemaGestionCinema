 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import java.io.Serializable;
/**
 *
 * @author Jollja
 */
public class Usuario implements Serializable{
    
    
    private int codigo;
    private String nombres;
    private String apellidoP;
    private String apellidoM;
    private String dni;
    private String contrasenia;
    private int idPerfil;
    
    public Usuario(String nombres, String apellidoP,String apellidoM, String dni, String contrasenia) {
        this.nombres = nombres;
        this.apellidoP = apellidoP;
        this.apellidoM=apellidoM;
        this.dni = dni;
        this.contrasenia = contrasenia;
    }

    public Usuario(String nombres, String apellidoP,String apellidoM, String dni, String contrasenia, int idPerfil) {
        this.nombres = nombres;
        this.apellidoP = apellidoP;
        this.apellidoM=apellidoM;
        this.dni = dni;
        this.contrasenia = contrasenia;
        this.idPerfil = idPerfil;
    }
    
    public Usuario(int codigo, String nombres, String apellidoP,String apellidoM, String dni, String contrasenia, int idPerfil) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.apellidoP = apellidoP;
        this.apellidoM=apellidoM;
        this.dni = dni;
        this.contrasenia = contrasenia;
        this.idPerfil = idPerfil;
    }
    
    public Usuario(int codigo, String nombres, String apellidoP,String apellidoM, String id, String contrasenia) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.apellidoP = apellidoP;
        this.apellidoM = apellidoM;
        this.dni = dni;
        this.contrasenia = contrasenia;
    }

    public int getCodigo() {
        return codigo;
    }
    
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getApellidoM() {
        return apellidoM;
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = apellidoM;
    }
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    
    public int getIdPerfil() {
        return idPerfil;
    }
    public void setIdPerfil(int idPerfil) {    
        this.idPerfil = idPerfil;
    }

    @Override
    public String toString() {
        return "Usuario{" + "codigo=" + codigo + ", nombres=" + nombres + ", apellidoP=" + apellidoP + ", apellidoM=" + apellidoM + ", dni=" + dni + ", contrase\u00f1a=" + contrasenia + '}';
    }
    
    
}