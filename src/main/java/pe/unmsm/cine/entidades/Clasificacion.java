/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Clasificacion {
    private int codigo;
    private String tipo;
    
    public Clasificacion(int codigo, String tipo){
        this.codigo=codigo;
        this.tipo=tipo;
    }
    
    public Clasificacion(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.tipo = json.getString(tipo);
    }
    
    public int getCodigo(){
        return codigo;
    }
    public void SetCodigo(){
        this.codigo=codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("tipo", tipo);
        return j;
    }
    
    public String toString(){
        return "Clasificacion {"+ "codigo=" +codigo + ",tipo=" +tipo + '}';
    }
}

