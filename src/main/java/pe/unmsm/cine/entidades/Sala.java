/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templatesx
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Sala {
    private int codigo;
    private TipoSala tipo;
    private int nroSala;
    private int nroAsientos;
    private int cantMaxPersonas;
    private Cine cine;

    public Sala(int nroSala, int nroAsientos,int cantMaxPersonas,TipoSala tipo, Cine cine) {
        this.nroAsientos=nroAsientos;
        this.nroSala = nroSala;
        this.cantMaxPersonas = cantMaxPersonas;
        this.tipo = tipo;        
    }
    
    
public Sala(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.tipo=new TipoSala(json.getJSONObject("tipo"));
        this.nroSala = json.getInt("nroSala");
        this.nroAsientos = json.getInt("nroAsientos");
        this.cantMaxPersonas = json.getInt("cantMaxPersonas");
        this.cine=new Cine(json.getJSONObject("cine"));
    }

    public Sala (int codigo,int nroSala,int nroAsientos, int cantMaxPersonas,TipoSala tipo, Cine cine) {
        this.codigo = codigo;
        this.nroSala = nroSala;
        this.nroAsientos=nroAsientos;
        this.cantMaxPersonas = cantMaxPersonas;
        this.tipo=tipo;
        this.cine=cine;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getNroSala() {
        return nroSala;
    }

    public void setNroSala(int nroSala) {
        this.nroSala = nroSala;
    }

    public int getNroAsientos() {
        return nroAsientos;
    }

    public void setNroAsientos(int nroAsientos) {
        this.nroAsientos = nroAsientos;
    }

    
    public int getCantMaxPersonas() {
        return cantMaxPersonas;
    }

    public void setCantMaxPersonas(int cantMaxPersonas) {
        this.cantMaxPersonas = cantMaxPersonas;
    }

    public Cine getCine(){
        return cine;
    }

    public void setCine(Cine cine) {
        this.cine = cine;
    }

    public TipoSala getTipo() {
        return tipo;
    }

    public void setTipo(TipoSala tipo) {
        this.tipo = tipo;
    }
    
    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("tipo", tipo);
        j.put("nroSala", nroSala);
        j.put("nroAsiento", nroAsientos);
        j.put("cantMaxPersonas", cantMaxPersonas);
        j.put("cine", cine);
        return j;
    }

    @Override
    public String toString() {
        return "Sala{" + "codigo=" + codigo +  ", tipo=" + tipo + ", nroSala=" +nroSala + ", cantMaxPersonas=" +cantMaxPersonas + ", cine=" +cine + '}';
    }
    
}
