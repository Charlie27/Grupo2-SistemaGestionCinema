/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class TipoSala {
    private int codigo;
    private String modelo;// si es 3d, 2d o xtream;
    private String descripcion;// por si deseamos agregar cosas
    
    public TipoSala(int codigo, String modelo,String descripcion){
        this.modelo=modelo;
        this.descripcion=descripcion;
    }
    public TipoSala(int codigo, String modelo){
        this.codigo=codigo;
        this.modelo=modelo;
    }
    
    
    public TipoSala(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.modelo = json.getString("modelo");
        this.descripcion = json.getString("descripcion");
    }
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("modelo", modelo);
        j.put("descripcion", descripcion);
        return j;
    }

    @Override
    public String toString() {
        return "TipoSala{" + "codigo=" + codigo + ", modelo=" + modelo + ", descripcion=" + descripcion + '}';
    }   
}
