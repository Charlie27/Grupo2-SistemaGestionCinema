/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class PeliculaActor {
    private int codigo;
    private Pelicula pelicula;
    private Actor actor;
    
    public PeliculaActor(int codigo, Pelicula pelicula,Actor actor){
        this.codigo=codigo;
        this.pelicula=pelicula;
        this.actor=actor;
    }  

    public PeliculaActor(Pelicula pelicula, Actor actor) {
        this.pelicula = pelicula;
        this.actor = actor;
    }
    
    public PeliculaActor(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.pelicula=new Pelicula(json.getJSONObject("pelicula"));
        this.actor=new Actor(json.getJSONObject("actor"));
        
    }
    
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("pelicula", pelicula);
        j.put("actor", actor);
        return j;
    }
    
    @Override
    public String toString() {
        return "PeliculaActor{" + "codigo=" + codigo + ", pelicula=" + pelicula + ", actor=" + actor + '}';
    }
    
}
