/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Asiento {
    private int codigo;
    private String fila;
    private int columna;
    private int ocupado;
    private int idSala;
    
    public Asiento(int codigo, String fila, int columna, int ocupado, int idSala){
        this.fila=fila;
        this.columna=columna;
        this.ocupado=ocupado;
        this.idSala=idSala;
    }
    
    
    public Asiento(JSONObject json)throws JSONException{
        this.codigo = json.getInt("codigo");
        this.fila = json.getString("fila");
        this.columna = json.getInt("columna");
        this.ocupado = json.getInt("ocupado");
        this.idSala= json.getInt("idSala");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getOcupado() {
        return ocupado;
    }

    public void setOcupado(int ocupado) {
        this.ocupado = ocupado;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    @Override
    public String toString() {
        return "Asiento{" + "codigo=" + codigo + ", fila=" + fila + ", columna=" + columna + ", ocupado=" + ocupado + ", idSala=" + idSala + '}';
    }
    
    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("fila",fila);
        j.put("columna", columna);
        j.put("ocupado", ocupado);
        j.put("idSala", idSala);
        return j;
    }
}
