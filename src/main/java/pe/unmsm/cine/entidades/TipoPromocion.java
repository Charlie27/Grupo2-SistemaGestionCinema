/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class TipoPromocion {
    private int codigo;
    private String promocion;
   
    public TipoPromocion(int codigo , String promocion){
        this.codigo=codigo;
        this.promocion=promocion;
    }
    
    
    public TipoPromocion(JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.promocion = json.getString("promocion");
    }
    
    public int getCodigo(){
        return codigo;
    }
    public void setCodigo(int codigo){
        this.codigo=codigo;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setPromocion(String promocion) {
        this.promocion=promocion;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("promocion", promocion);
        return j;
    }
    
    @Override
    public String toString() {
        return "Dia{" + "codigo=" + codigo + ", promocion=" + promocion + '}';
    }
    
}
