/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import java.io.Serializable;
/**
 *
 * @author Jollja
 */
public class Empleado implements Serializable{
    private int codigo;
    private String nombres;
    private String apellidoP;
    private String apellidoM;
    private String dni;
    private String contrasenia;
    private int idPerfil;
    private int idEmpleado;
    private String foto;
    private int idCine;

    public Empleado(int codigo, String nombres, String apellidoP, String apellidoM, String dni, String contrasenia, int idEmpleado, int idPerfil, String foto, int idCine) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.apellidoP = apellidoP;
        this.apellidoM = apellidoM;
        this.dni = dni;
        this.contrasenia = contrasenia;
        this.idPerfil = idPerfil;
        this.idEmpleado = idEmpleado;
        this.foto = foto;
        this.idCine = idCine;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getApellidoM() {
        return apellidoM;
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = apellidoM;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getIdCine() {
        return idCine;
    }

    public void setIdCine(int idCine) {
        this.idCine = idCine;
    }

    @Override
    public String toString() {
        return "Empleado{" + "codigo=" + idEmpleado + ", rutadefoto=" + foto + ", idCine=" + idCine + '}';
    }
    
    
}
