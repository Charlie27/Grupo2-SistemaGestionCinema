/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Entrada {
    private Venta venta;
    private Asiento asiento;
    private Boleto boleto;
    
    public Entrada(Venta venta, Asiento asiento, Boleto boleto){
        this.venta=venta;
        this.asiento=asiento;
        this.boleto=boleto;
    }
    
    
    public Entrada(JSONObject json) throws JSONException{
        //this.venta=new Venta(json.getJSONObject("venta"));
        this.asiento=new Asiento(json.getJSONObject("asiento"));
        this.boleto=new Boleto(json.getJSONObject("boleto"));
        
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Asiento getAsiento() {
        return asiento;
    }

    public void setAsiento(Asiento asiento) {
        this.asiento = asiento;
    }

    public Boleto getBoleto() {
        return boleto;
    }

    public void setBoleto(Boleto boleto) {
        this.boleto = boleto;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("venta", venta);
        j.put("asiento", asiento);
        j.put("boleto", boleto);
        return j;
    }
    
    @Override
    public String toString() {
        return "Entrada{" + "venta=" + venta + ", asiento=" + asiento + ", boleto=" + boleto + '}';
    }
    
    
}
