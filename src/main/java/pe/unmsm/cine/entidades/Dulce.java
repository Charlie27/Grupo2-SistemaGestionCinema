/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Dulce {
    private int codigo;
    private String nombre;
    private int precio;
    private String tamaño;
    private String img;
    private String descripcion;
    
    
public Dulce(int codigo, String nombre, int precio,String tamaño,String img,String descripcion){
    this.codigo=codigo;
    this.nombre=nombre;
    this.precio=precio;
    this.tamaño=tamaño;
    this.img=img;
    this.descripcion=descripcion;
}


public Dulce(JSONObject json) throws JSONException{
    this.codigo = json.getInt("codigo");
    this.nombre = json.getString("nombre");
    this.precio = json.getInt("precio");
    this.tamaño = json.getString("tamaño");
    this.img = json.getString("img");
    this.descripcion = json.getString("descripcion");
    }


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("nombre", nombre);
        j.put("precio", precio);
        j.put("tamaño", tamaño);
        j.put("img",img);
        j.put("descripcion", descripcion);
        return j;
    }
    
    @Override
    public String toString() {
        return "Dulce{" + "codigo=" + codigo + ", nombre=" + nombre + ", precio=" + precio + ", tama\u00f1o=" + tamaño + ", descripcion=" + descripcion + '}';
    }
    

}


