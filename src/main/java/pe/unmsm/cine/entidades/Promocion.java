/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.entidades;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jollja
 */
public class Promocion {
    private int codigo;
    private String nombre;
    private int precio;
    private String img; // la base de datos hay que agregarle una descripcion de la prmocion.
    private TipoPromocion tpromo;
    
    public Promocion(int codigo, String nombre,int precio,String img,TipoPromocion tpromo ){
        this.codigo=codigo;
        this.nombre=nombre;
        this.precio=precio;
        this.img=img;
        this.tpromo=tpromo;
    }
    
    public Promocion (JSONObject json) throws JSONException{
        this.codigo = json.getInt("codigo");
        this.nombre = json.getString("nombre");
        this.precio = json.getInt("precio");
        this.img = json.getString("img");
        this.tpromo=new TipoPromocion(json.getJSONObject("tpromo"));
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public TipoPromocion getTpromo() {
        return tpromo;
    }

    public void setTpromo(TipoPromocion tpromo) {
        this.tpromo = tpromo;
    }

    public JSONObject getJson(){
        JSONObject j = new JSONObject();
        j.put("codigo", codigo);
        j.put("nombre", nombre);
        j.put("precio", precio);
        j.put("img", img);
        j.put("tpromo", tpromo);
        return j;
    }
    
    @Override
    public String toString() {
        return "Promocion{" + "codigo=" + codigo + ", nombre=" + nombre + ", precio=" + precio + ", img=" + img + ", tpromo=" + tpromo + '}';
    }
    
    
}
