package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.VentaPromocion;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class VentaPromocionesDAO implements AbstractDAO<VentaPromocion,Integer>{

    @Override
    public boolean registrar(VentaPromocion venta_promocion) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO VENTAS_PROMOCIONES (id_venta,id_promocion) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, venta_promocion.getVenta().getCodigo());
            sentPrep.setInt(2, venta_promocion.getPromocion().getCodigo());
            
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(VentaPromocion venta_promocion) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE VENTAS_PROMOCIONES "
               + "id_venta = ?, "
               + "id_promocion = ?, "
               + "WHERE id_ventas_promocions = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, venta_promocion.getPromocion().getCodigo());
            sentPrep.setInt(2, venta_promocion.getPromocion().getCodigo());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public VentaPromocion get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM VENTAS_PROMOCIONES WHERE id_ventas_promocions= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            VentaPromocion venta_promocion = null;
            if (rs.next()) {
                int cod = rs.getInt("id_ventas_promocions");
                int venta = rs.getInt("id_venta");
                int promocion = rs.getInt("id_promocion");
                
                venta_promocion = new VentaPromocion(cod,new VentasDAO().get(venta),new PromocionesDAO().get(promocion));
            }

            sentPrep.close();
            rs.close();

            return venta_promocion;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<VentaPromocion> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<VentaPromocion> listaVentaPromociones = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM VENTAS_PROMOCIONES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_ventas_promocions");
                int venta = rs.getInt("id_venta");
                int promocion = rs.getInt("id_promocion");

                listaVentaPromociones.insertarAlFinal(new VentaPromocion(cod,new VentasDAO().get(venta),new PromocionesDAO().get(promocion)));
            }

            rs.close();
            sentPrep.close();

            return listaVentaPromociones;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM VENTAS_PROMOCIONES WHERE id_ventas_promocions = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
