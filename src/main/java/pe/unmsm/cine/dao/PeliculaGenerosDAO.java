
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.PeliculaGenero;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class PeliculaGenerosDAO implements AbstractDAO<PeliculaGenero,Integer>{

    @Override
    public boolean registrar(PeliculaGenero peli_genero) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO PELICULAS_GENEROS (id_peliculas,id_genero) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, peli_genero.getPelicula().getCodigo());
            sentPrep.setInt(2, peli_genero.getGenero().getCodigo());
            
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(PeliculaGenero peli_genero) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE PELICULAS_GENEROS "
               + "id_pelicula = ?, "
               + "id_genero = ?, "
               + "WHERE id_peliculas_generos = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, peli_genero.getPelicula().getCodigo());
            sentPrep.setInt(2, peli_genero.getGenero().getCodigo());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public PeliculaGenero get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM PELICULAS_GENEROS WHERE id_peliculas_generos= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            PeliculaGenero peli_genero = null;
            if (rs.next()) {
                int cod = rs.getInt("id_peliculas_generos");
                int peli = rs.getInt("id_pelicula");
                int genero = rs.getInt("id_genero");
                
                peli_genero = new PeliculaGenero(cod,new PeliculasDAO().get(peli),new GenerosDAO().get(genero));
            }

            sentPrep.close();
            rs.close();

            return peli_genero;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<PeliculaGenero> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<PeliculaGenero> listaPeliculaGeneros = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM PELICULAS_GENEROS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_peliculas_generos");
                int peli = rs.getInt("id_pelicula");
                int genero = rs.getInt("id_genero");

                listaPeliculaGeneros.insertarAlFinal(new PeliculaGenero(cod,new PeliculasDAO().get(peli),new GenerosDAO().get(genero)));
            }

            rs.close();
            sentPrep.close();

            return listaPeliculaGeneros;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM PELICULAS_GENEROS WHERE id_peliculas_generos = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
