/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Cine;
import pe.unmsm.cine.entidades.Ciudad;
import pe.unmsm.cine.entidades.Empleado;
import pe.unmsm.cine.entidades.Perfil;
import pe.unmsm.cine.entidades.Usuario;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class EmpleadoDAO implements AbstractDAO<Empleado,Integer>{

    public Empleado registrar(String name, String apePat, String apeMat, String dni, String pssw, String foto, int idCine, int idPerfil) {
        try {
            Connection conn = AccesoDB.getConnection();

            CallableStatement call = conn.prepareCall("{ call register_employee(?,?,?,?,?,?,?,?) }");
            
            call.setString(1, name);
            call.setString(2, apePat);
            call.setString(3, apeMat);
            call.setString(4, dni);
            call.setString(5, pssw);
            call.setString(6, foto);
            call.setInt(7, idCine);
            call.setInt(8, idPerfil);
            
            ResultSet rs = call.executeQuery();
            Empleado empleado = null;
            if (rs.next()) {
                //Tabla empleados
                int idEmpleado = rs.getInt("id_empleado");
                String ruta=rs.getString("foto");
                int id_cine = rs.getInt("id_cine");
                
                //Tabla usuarios
                int idUsuario= rs.getInt("id_usuario");
                String nombres = rs.getString("nombres");
                String apellidop = rs.getString("apellido_paterno");
                String apellidom = rs.getString("apellido_materno");
                String DNI = rs.getString("dni");
                String contraseña = rs.getString("password");
                int id_perfil = rs.getInt("id_perfil");
                
                empleado = new Empleado(idUsuario,nombres,apellidop,apellidom,dni,contraseña,id_perfil,idEmpleado,ruta,id_cine);
            }
            call.close();
            rs.close();
            return empleado;

        } catch (SQLException t) {
            System.out.println(t);
            return null;
        }
    }

    @Override
    public boolean modificar(Empleado empleado) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE EMPLEADOS "
               +"foto = ? " 
               + "id_cine = ? "
               + "id_usuario= ? "
               + "WHERE id_distrito = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, empleado.getFoto());
            sentPrep.setInt(2, empleado.getIdCine());
            sentPrep.setInt(3, empleado.getCodigo());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public Empleado get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM EMPLEADOS NATURAL JOIN USUARIOS WHERE id_empleado= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Empleado empleado = null;
            if (rs.next()) {
                //Tabla empleados
                int idEmpleado = rs.getInt("id_empleado");
                String ruta=rs.getString("foto");
                int idCine = rs.getInt("id_cine");
                
                //Tabla usuarios
                int idUsuario= rs.getInt("id_usuario");
                String nombres = rs.getString("nombres");
                String apellidop = rs.getString("apellido_paterno");
                String apellidom = rs.getString("apellido_materno");
                String dni = rs.getString("dni");
                String contraseña = rs.getString("password");
                int idPerfil = rs.getInt("id_perfil");
                
                empleado = new Empleado(idUsuario,nombres,apellidop,apellidom,dni,contraseña,idPerfil,idEmpleado,ruta,idCine);
            }

            sentPrep.close();
            rs.close();

            return empleado;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Empleado> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Empleado> listaEmpleados = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM EMPLEADOS NATURAL JOIN USUARIOS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_empleado");
                String ruta = rs.getString("foto");
                int cine = rs.getInt("id_cine");
                
                //Tabla usuarios
                int idUsuario= rs.getInt("id_usuario");
                String nombres = rs.getString("nombres");
                String apellidop = rs.getString("apellido_paterno");
                String apellidom = rs.getString("apellido_materno");
                String dni = rs.getString("dni");
                String contraseña = rs.getString("password");
                int idPerfil = rs.getInt("id_perfil");
                
                listaEmpleados.insertarAlFinal(new Empleado(idUsuario,nombres,apellidop,apellidom,dni,contraseña,idPerfil, cod,ruta,cine));
            }

            rs.close();
            sentPrep.close();

            return listaEmpleados;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM USUARIOS WHERE id_usuario = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }
    
    public Empleado getByUserId(Integer id) {
        try {
                Connection conn = AccesoDB.getConnection();

                PreparedStatement sentPrep = null;

                ResultSet rs = null;

                String sentencia = "SELECT * FROM EMPLEADOS NATURAL JOIN USUARIOS NATURAL JOIN PERFILES NATURAL JOIN CINES NATURAL JOIN CIUDAD WHERE id_usuario= ?";

                sentPrep = conn.prepareStatement(sentencia);
                sentPrep.setInt(1, id);

                rs = sentPrep.executeQuery();

                Empleado empleado = null;
                if (rs.next()) {
                    //Tabla empleados
                    int idEmpleado = rs.getInt("id_empleado");
                    String ruta=rs.getString("foto");
                    int idCine = rs.getInt("id_cine");

                    //Tabla usuarios
                    int idUsuario= rs.getInt("id_usuario");
                    String nombres = rs.getString("nombres");
                    String apellidop = rs.getString("apellido_paterno");
                    String apellidom = rs.getString("apellido_materno");
                    String dni = rs.getString("dni");
                    String contraseña = rs.getString("password");
                    int idPerfil = rs.getInt("id_perfil");

                    empleado = new Empleado(idUsuario,nombres,apellidop,apellidom,dni,contraseña,idPerfil, idEmpleado,ruta, idCine);
                }

                sentPrep.close();
                rs.close();

                return empleado;
            } catch (SQLException e) {
                System.out.println(e);
            }
            return null;
        }

    @Override
    public boolean registrar(Empleado e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
