/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Ciudad;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class CiudadDAO implements AbstractDAO<Ciudad,Integer>{

    @Override
    public boolean registrar(Ciudad ciudad) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO CIUDAD (nombre_ciudad) VALUES (?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, ciudad.getNombre());
            

            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;    
    }

    @Override
    public boolean modificar(Ciudad ciudad) {
      try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE CIUDAD "
               + "nombre_ciudad = ? "
               + "WHERE id_ciudad = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, ciudad.getNombre());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public Ciudad get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM CIUDAD WHERE id_ciudad= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Ciudad ciudad = null;
            if (rs.next()) {
                int cod = rs.getInt("id_ciudad");
                String nombre = rs.getString("nombre_ciudad");
                
                ciudad = new Ciudad(cod,nombre);
            }

            sentPrep.close();
            rs.close();

            return ciudad;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Ciudad> getAll() {
      try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Ciudad> listaDistritos = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT id_ciudad, nombre_ciudad FROM CIUDAD";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_ciudad");
                String nombre = rs.getString("nombre_ciudad");
                
                listaDistritos.insertarAlFinal(new Ciudad(cod,nombre));
            }

            rs.close();
            sentPrep.close();

            return listaDistritos;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
        try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM CIUDAD WHERE id_ciudad = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
