/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Asiento;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class AsientosDAO implements AbstractDAO<Asiento,Integer> {

    @Override
    public boolean registrar(Asiento asiento) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO ASIENTOS (fila , columna ,ocupado,id_sala) VALUES (?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, asiento.getFila());
            sentPrep.setInt(2, asiento.getColumna());
            sentPrep.setInt(3, asiento.getOcupado());
            sentPrep.setInt(4,asiento.getIdSala()); // no se si ponerle getCodigo y ahi vemos el codigo de la sala
                                                              // o llamar a getNombre y tener el nombre de la sala esa es mi duda.
                                                              //Si ven algo parecido es que tenia las mismas dudas en todas.

            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Asiento asiento) {
        try {
// (fila ,columna,ocupado,id_sala)

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;
            String sentencia = "UPDATE  ASIENTOS"
                    + "SET fila = ?,"
                    + "columna = ?,"
                    + "ocupado= ?,"
                    + "id_sala= ?,"
                    + "WHERE id_asiento = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, asiento.getFila());
            sentPrep.setInt(2, asiento.getColumna());
            sentPrep.setInt(3, asiento.getOcupado());
            sentPrep.setInt(4, asiento.getIdSala());

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public Asiento get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM ASIENTOS WHERE id_cine= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Asiento asiento = null;
            if (rs.next()) {
                int cod = rs.getInt("id_asiento");
                String fila = rs.getString("fila");
                int columna = rs.getInt("columna");
                int estado = rs.getInt("ocupado");
                int sala = rs.getInt("id_sala");  
                asiento = new Asiento(cod,fila,columna,estado,sala);
            }

            sentPrep.close();
            rs.close();

            return asiento;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Asiento> getAll() {
        try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Asiento> listaAsientos = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM ASIENTOS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_asiento");
                String fila = rs.getString("fila");
                int columna = rs.getInt("columna");
                int estado = rs.getInt("ocupado");
                int sala = rs.getInt("id_sala");  
                listaAsientos.insertarAlFinal(new Asiento(cod,fila,columna,estado,sala));
            }

            rs.close();
            sentPrep.close();

            return listaAsientos;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM ASIENTOS WHERE id_asiento = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;   
    }
    
}
