/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Promocion;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class PromocionesDAO implements AbstractDAO<Promocion, Integer>{

    @Override
    public boolean registrar(Promocion promocion) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO PROMOCIONES (nombre,precio,promocion_img,id_tipo_promocion) VALUES (?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1,promocion.getNombre());
            sentPrep.setInt(2, promocion.getPrecio());
            sentPrep.setString(3, promocion.getImg());
            sentPrep.setInt(4, promocion.getTpromo().getCodigo());
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Promocion promocion) {
     try {

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;

            String sentencia = "UPDATE PROMOCIONES "
                     + "SET nombre = ?,"
                     + "precio= ?,"
                     + "promocion_img = ?, "
                     + "id_tipo_promocion"
                     + "WHERE id_promocion = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, promocion.getNombre());
            sentPrep.setInt(2, promocion.getPrecio());
            sentPrep.setString(3, promocion.getImg());
            sentPrep.setInt(4, promocion.getTpromo().getCodigo());
           

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }   

    @Override
    public Promocion get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM PROMOCIONES WHERE id_promocion= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Promocion promocion = null;
            if (rs.next()) {
                int cod = rs.getInt("id_promocion");
                String nombre = rs.getString("nombre");
                int precio=rs.getInt("precio");
                String promo_img = rs.getString("promocion_img");
                int tipo_promo = rs.getInt("id_tipo_promocion");
                
                promocion = new Promocion(cod,nombre,precio,promo_img,new TipoPromocionesDAO().get(tipo_promo));
            }

            sentPrep.close();
            rs.close();

            return promocion;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null; 
    }

    @Override
    public ListaDoble<Promocion> getAll() {
    try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Promocion> listaPromociones = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM PROMOCIONES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_promocion");
                String nombre = rs.getString("nombre");
                int precio=rs.getInt("precio");
                String promo_img = rs.getString("promocion_img");
                int tipo_promo = rs.getInt("id_tipo_promocion");
                
                
                listaPromociones.insertarAlFinal(new Promocion(cod,nombre,precio,promo_img,new TipoPromocionesDAO().get(tipo_promo)));
            }

            rs.close();
            sentPrep.close();

            return listaPromociones;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;    
    }

    @Override
    public boolean eliminar(Integer id) {
        try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM PROMOCIONES WHERE id_promocion = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

}
