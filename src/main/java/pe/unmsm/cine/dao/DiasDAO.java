
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Dia;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class DiasDAO implements AbstractDAO<Dia,Integer>{

    @Override
    public boolean registrar(Dia dia) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO DIAS (tipo) VALUES (?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, dia.getDia());
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Dia dia) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE DIAS "
               + "dia = ? "
               + "WHERE id_dia = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, dia.getDia());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public Dia get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM DIAS WHERE id_dia= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Dia dia = null;
            if (rs.next()) {
                int cod = rs.getInt("id_dia");
                String diasemana = rs.getString("dia");
                
                dia = new Dia(cod,diasemana);
            }

            sentPrep.close();
            rs.close();

            return dia;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Dia> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Dia> listaDias = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM DIAS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_dia");
                String diasemana = rs.getString("dia");
                
                listaDias.insertarAlFinal(new Dia(cod,diasemana));
            }

            rs.close();
            sentPrep.close();

            return listaDias;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM DIAS WHERE id_dia = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
