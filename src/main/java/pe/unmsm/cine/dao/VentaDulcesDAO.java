package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.VentaDulce;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class VentaDulcesDAO implements AbstractDAO<VentaDulce,Integer>{

    @Override
    public boolean registrar(VentaDulce venta_dulce) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO VENTAS_DULCES (id_venta,id_dulce) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, venta_dulce.getVenta().getCodigo());
            sentPrep.setInt(2, venta_dulce.getDulce().getCodigo());
            
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(VentaDulce venta_dulce) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE VENTAS_DULCES "
               + "id_venta = ?, "
               + "id_dulce = ?, "
               + "WHERE id_ventas_dulces = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, venta_dulce.getDulce().getCodigo());
            sentPrep.setInt(2, venta_dulce.getDulce().getCodigo());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public VentaDulce get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM VENTAS_DULCES WHERE id_ventas_dulces= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            VentaDulce venta_dulce = null;
            if (rs.next()) {
                int cod = rs.getInt("id_ventas_dulces");
                int venta = rs.getInt("id_venta");
                int dulce = rs.getInt("id_dulce");
                
                venta_dulce = new VentaDulce(cod,new VentasDAO().get(venta),new DulcesDAO().get(dulce));
            }

            sentPrep.close();
            rs.close();

            return venta_dulce;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<VentaDulce> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<VentaDulce> listaVentaDulces = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM VENTAS_DULCES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_ventas_dulces");
                int venta = rs.getInt("id_venta");
                int dulce = rs.getInt("id_dulce");

                listaVentaDulces.insertarAlFinal(new VentaDulce(cod,new VentasDAO().get(venta),new DulcesDAO().get(dulce)));
            }

            rs.close();
            sentPrep.close();

            return listaVentaDulces;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM VENTAS_DULCES WHERE id_ventas_dulces = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
