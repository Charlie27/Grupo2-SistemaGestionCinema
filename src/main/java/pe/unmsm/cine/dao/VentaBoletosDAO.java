
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.VentaBoleto;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class VentaBoletosDAO implements AbstractDAO<VentaBoleto,Integer>{

    @Override
    public boolean registrar(VentaBoleto venta_boleto) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO VENTAS_BOLETOS (id_venta,id_boleto) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, venta_boleto.getVenta().getCodigo());
            sentPrep.setInt(2, venta_boleto.getBoleto().getCodigo());
            
                        
            sentPrep.executeUpdate();
            conn.close();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(VentaBoleto venta_boleto) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE VENTAS_BOLETOS "
               + "id_venta = ?, "
               + "id_boleto = ?, "
               + "WHERE id_ventas_boletos = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, venta_boleto.getBoleto().getCodigo());
            sentPrep.setInt(2, venta_boleto.getBoleto().getCodigo());
            sentPrep.executeUpdate();

            conn.close();
            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public VentaBoleto get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM VENTAS_BOLETOS WHERE id_ventas_boletos= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            VentaBoleto venta_boleto = null;
            if (rs.next()) {
                int cod = rs.getInt("id_ventas_boletos");
                int venta = rs.getInt("id_venta");
                int boleto = rs.getInt("id_boleto");
                
                venta_boleto = new VentaBoleto(cod,new VentasDAO().get(venta),new BoletosDAO().get(boleto));
            }

            conn.close();
            sentPrep.close();
            rs.close();

            return venta_boleto;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<VentaBoleto> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<VentaBoleto> listaVentaBoletos = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM VENTAS_BOLETOS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_ventas_boletos");
                int venta = rs.getInt("id_venta");
                int boleto = rs.getInt("id_boleto");

                listaVentaBoletos.insertarAlFinal(new VentaBoleto(cod,new VentasDAO().get(venta),new BoletosDAO().get(boleto)));
            }

            conn.close();
            rs.close();
            sentPrep.close();

            return listaVentaBoletos;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM VENTAS_BOLETOS WHERE id_ventas_boletos = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            conn.close();
            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
