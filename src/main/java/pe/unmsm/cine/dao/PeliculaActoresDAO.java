
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.PeliculaActor;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class PeliculaActoresDAO implements AbstractDAO<PeliculaActor,Integer>{

    @Override
    public boolean registrar(PeliculaActor peli_actor) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO PELICULAS_ACTORES (id_peliculas,id_actor) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, peli_actor.getPelicula().getCodigo());
            sentPrep.setInt(2, peli_actor.getActor().getCodigo());
            
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(PeliculaActor peli_actor) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE PELICULAS_ACTORES "
               + "id_pelicula = ?, "
               + "id_actor = ?, "
               + "WHERE id_peliculas_actores = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, peli_actor.getPelicula().getCodigo());
            sentPrep.setInt(2, peli_actor.getActor().getCodigo());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public PeliculaActor get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM PELICULAS_ACTORES WHERE id_peliculas_actores= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            PeliculaActor peli_actor = null;
            if (rs.next()) {
                int cod = rs.getInt("id_peliculas_actores");
                int peli = rs.getInt("id_pelicula");
                int actor = rs.getInt("id_actor");
                
                peli_actor = new PeliculaActor(cod,new PeliculasDAO().get(peli),new ActoresDAO().get(actor));
            }

            sentPrep.close();
            rs.close();

            return peli_actor;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<PeliculaActor> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<PeliculaActor> listaPeliculaActores = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM PELICULAS_ACTORES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_peliculas_actores");
                int peli = rs.getInt("id_pelicula");
                int actor = rs.getInt("id_actor");

                listaPeliculaActores.insertarAlFinal(new PeliculaActor(cod,new PeliculasDAO().get(peli),new ActoresDAO().get(actor)));
            }

            rs.close();
            sentPrep.close();

            return listaPeliculaActores;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM PELICULAS_ACTORES WHERE id_peliculas_actores = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
