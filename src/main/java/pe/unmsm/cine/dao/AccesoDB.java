/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jollja
 */
public class AccesoDB {
    private static Connection connection = null;
    
    private AccesoDB(){}
    
    private static void setUpConnection(){
        try {
            ComboPooledDataSource cpds = new ComboPooledDataSource();
            cpds.setDriverClass("com.mysql.jdbc.Driver");
            cpds.setJdbcUrl("jdbc:mysql://107.170.225.213:3306/SistemaGestionCinema?noAccessToProcedureBodies=true");
            cpds.setUser("cinema");
            cpds.setPassword("cinema");
            cpds.setMinPoolSize(3);
            cpds.setMaxPoolSize(30);
            cpds.setAcquireIncrement(1);
            cpds.setAutoCommitOnClose(false);
            cpds.setMaxIdleTime(20);
            cpds.setMaxIdleTimeExcessConnections(240);
            cpds.setCheckoutTimeout(10000);
            connection = cpds.getConnection();
        } catch (PropertyVetoException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(AccesoDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void startConecction(){
        setUpConnection();
    }
    
    public static Connection getConnection() throws SQLException{
        if(connection == null || connection.isClosed()) {
            setUpConnection();
            System.out.println("conecta");
        }
        return connection;
    }
    
}
