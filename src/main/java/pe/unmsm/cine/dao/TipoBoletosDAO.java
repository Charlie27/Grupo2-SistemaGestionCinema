/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.TipoBoleto;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class TipoBoletosDAO implements AbstractDAO<TipoBoleto,Integer>{

    @Override
    public boolean registrar(TipoBoleto tipo) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO TIPO_BOLETO (tipo_boleto,descripcion) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, tipo.getModelo());
            sentPrep.setString(2,tipo.getDescripcion());
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(TipoBoleto tipo) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE TIPO_BOLETO "
               + "tipo_boleto = ?"
               +"descripcion= ? "
               + "WHERE id_tipo_boleto = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, tipo.getModelo());
            sentPrep.setString(2, tipo.getDescripcion());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public TipoBoleto get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM TIPO_BOLETO WHERE id_tipo_boleto= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            TipoBoleto tipo = null;
            if (rs.next()) {
                int cod = rs.getInt("id_tipo_boleto");
                String modelo = rs.getString("tipo_boleto");
                String descripcion = rs.getString("descripcion");
                
                tipo = new TipoBoleto(cod,modelo,descripcion);
            }

            sentPrep.close();
            rs.close();

            return tipo;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<TipoBoleto> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<TipoBoleto> listaBoletos = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM TIPO_BOLETO";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_tipo_boleto");
                String modelo = rs.getString("tipo_boleto");
                String descripcion = rs.getString("descripcion");
                listaBoletos.insertarAlFinal(new TipoBoleto(cod,modelo,descripcion));
            }

            rs.close();
            sentPrep.close();

            return listaBoletos;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM TIPO_BOLETO WHERE id_tipo_boleto = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
