/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Dulce;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class DulcesDAO implements AbstractDAO<Dulce,Integer>{
    @Override
    public boolean registrar(Dulce dulce) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO DULCES (nombre,precio,tamaño,img,descripcion) VALUES (?,?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, dulce.getNombre());
            sentPrep.setInt(2, dulce.getPrecio());
            sentPrep.setString(3,dulce.getTamaño());
            sentPrep.setString(4, dulce.getImg());
            sentPrep.setString(5, dulce.getDescripcion());
            

            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Dulce dulce) {
        try {

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;
//(nombre,precio,tamaño,descripcion)
            String sentencia = "UPDATE DULCES "
                    + "SET nombre = ?,"
                    + "precio = ?, "
                    + "tamaño = ?, "
                    +"img = ?, "
                    + "descripcion = ?, "
                   
                    + "WHERE id_dulce = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, dulce.getNombre());
            sentPrep.setInt(2, dulce.getPrecio());
            sentPrep.setString(3, dulce.getTamaño());
            sentPrep.setString(4, dulce.getImg());
            sentPrep.setString(5, dulce.getDescripcion());

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public Dulce get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM DULCES WHERE id_dulce= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Dulce dulce = null;
            if (rs.next()) {
                int cod = rs.getInt("id_dulce");
                String nombre = rs.getString("nombre");
                int precio = rs.getInt("precio");
                String tamaño = rs.getString("tamaño");
                String img = rs.getString("img");
                String descripcion = rs.getString("descripcion");
                
                dulce = new Dulce(cod,nombre,precio,tamaño,img,descripcion);
            }

            sentPrep.close();
            rs.close();

            return dulce;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Dulce> getAll() {
        try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Dulce> listaDulces = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT id_dulce, nombre, precio, tamaño, img, descripcion FROM DULCES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_dulce");
                String nombre = rs.getString("nombre");
                int precio = rs.getInt("precio");
                String tamaño = rs.getString("tamaño");
                String img = rs.getString("img");
                String descripcion = rs.getString("descripcion");
                listaDulces.insertarAlFinal(new Dulce(cod,nombre,precio,tamaño,img,descripcion));
            }

            rs.close();
            sentPrep.close();

            return listaDulces;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
        try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM DULCES WHERE id_dulce = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            conn.close();
            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }
}
