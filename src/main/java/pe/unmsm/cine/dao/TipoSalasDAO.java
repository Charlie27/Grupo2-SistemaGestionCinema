/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.TipoSala;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class TipoSalasDAO implements AbstractDAO<TipoSala,Integer>{

    @Override
    public boolean registrar(TipoSala tipo) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO TIPOS_SALA (tipo) VALUES (?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, tipo.getModelo());
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(TipoSala tipo) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE TIPOS_SALA "
               + "tipo = ? "
               + "WHERE id_tipo_sala = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, tipo.getModelo());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public TipoSala get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM TIPOS_SALA WHERE id_tipo_sala= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            TipoSala tipo = null;
            if (rs.next()) {
                int cod = rs.getInt("id_tipo_sala");
                String modelo = rs.getString("tipo");
                
                tipo = new TipoSala(cod,modelo);
            }

            sentPrep.close();
            rs.close();

            return tipo;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<TipoSala> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<TipoSala> listaActores = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM TIPOS_SALA";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_tipo_sala");
                String modelo = rs.getString("tipo");
                
                listaActores.insertarAlFinal(new TipoSala(cod,modelo));
            }

            rs.close();
            sentPrep.close();

            return listaActores;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM TIPOS_SALA WHERE id_tipo_sala = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
