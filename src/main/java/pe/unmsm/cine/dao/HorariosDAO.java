/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Horario;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class HorariosDAO implements AbstractDAO<Horario, Integer>{

    @Override
    public boolean registrar(Horario horario) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO HORARIOS (id_cine,id_pelicula,id_sala,horario) VALUES (?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(0,horario.getCine().getCodigo());
            sentPrep.setInt(1, horario.getPelicula().getCodigo());
            sentPrep.setInt(2, horario.getSala().getCodigo());
            sentPrep.setInt(3, horario.getDuracion());
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Horario horario) {
     try {

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;

            String sentencia = "UPDATE HORARIOS "
                     + "SET id_cine = ?,"
                     + "id_pelicula= ?,"
                     + "id_sala = ?,"
                     + "horario =?,"
                     + "WHERE id_horarios = ?;";//debe haber un id horario

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(0,horario.getCine().getCodigo());
            sentPrep.setInt(1, horario.getPelicula().getCodigo());
            sentPrep.setInt(2, horario.getSala().getCodigo());
            sentPrep.setInt(3, horario.getDuracion());
           

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }   

    @Override
    public Horario get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM HORARIOS WHERE id_horarios= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Horario horario = null;
            if (rs.next()) {
                int cod = rs.getInt("id_horarios");
                int cine = rs.getInt("id_cine");
                int pelicula=rs.getInt("id_pelicula");
                int sala = rs.getInt("id_sala");
                int duracion = rs.getInt("horario");
                
                horario = new Horario(cod,new CinesDAO().get(cine),new PeliculasDAO().get(pelicula),new SalasDAO().get(sala),duracion);
            }

            sentPrep.close();
            rs.close();

            return horario;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null; 
    }

    @Override
    public ListaDoble<Horario> getAll() {
    try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Horario> listaHorarioes = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM HORARIOS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_horario");
                int cine = rs.getInt("id_cine");
                int pelicula=rs.getInt("id_pelicula");
                int sala = rs.getInt("id_sala");
                int duracion = rs.getInt("horario");
                
                
            
                listaHorarioes.insertarAlFinal(new Horario(cod,new CinesDAO().get(cine),new PeliculasDAO().get(pelicula),new SalasDAO().get(sala),duracion));
            }

            rs.close();
            sentPrep.close();

            return listaHorarioes;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;    
    }

    @Override
    public boolean eliminar(Integer id) {
        try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM HORARIOS WHERE id_horarios = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

}
