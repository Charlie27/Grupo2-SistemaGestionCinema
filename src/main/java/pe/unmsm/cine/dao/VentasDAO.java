/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Venta;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class VentasDAO implements AbstractDAO<Venta,Integer> {

    @Override
    public boolean registrar(Venta venta) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO VENTAS (nmr_venta , nmr_ventas ,aforo ,id_tipo_venta,id_cine) VALUES (?,?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setDouble(1, venta.getMonto());
//          sentPrep.setDate(2, venta.getFecha().addTimeOfDay(10, 20, 30, 40)); // corregir aqui cualquier cosa
            sentPrep.setInt(3, venta.getUsuario().getCodigo());


            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Venta venta) {
    try {
// (nmr_venta , nmr_ventas ,aforo ,id_tipo_venta,id_cine)

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;
            String sentencia = "UPDATE VENTAS "
                    + "SET monto = ?,"
                    + "fecha_venta = ?, "
                    + "id_usuario= ?,"
                    + "WHERE id_venta = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setDouble(1, venta.getMonto());
//          sentPrep.setDate(2, venta.getFecha().addTimeOfDay(10, 20, 30, 40)); // corregir aqui cualquier cosa
            sentPrep.setInt(3, venta.getUsuario().getCodigo());

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;    
    }

    @Override
    public Venta get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM VENTAS WHERE id_venta= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Venta venta = null;
            if (rs.next()) {
                int cod = rs.getInt("id_venta");
                double monto = rs.getDouble("monto");
                // Date    fecha_venta = rs.getDate("fecha_venta");
                int usuario = rs.getInt("usuario");
                
                venta = new Venta(cod, monto ,/* Date fecha_venta , */ new UsuarioDAO().get(usuario) );
            }

            sentPrep.close();
            rs.close();

            return venta;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Venta> getAll() {
    try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Venta> listaVentas = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM VENTAS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_venta");
                double monto = rs.getDouble("monto");
                // Date    fecha_venta = rs.getDate("fecha_venta");
                int usuario = rs.getInt("usuario");  
                listaVentas.insertarAlFinal(new Venta(cod, monto ,/* Date fecha_venta , */ new UsuarioDAO().get(usuario) ));
            }

            rs.close();
            sentPrep.close();

            return listaVentas;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
         try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM VENTAS WHERE id_venta = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }
    
}
