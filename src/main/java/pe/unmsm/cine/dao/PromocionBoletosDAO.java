
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.PromocionBoleto;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class PromocionBoletosDAO implements AbstractDAO<PromocionBoleto,Integer>{

    @Override
    public boolean registrar(PromocionBoleto promo_boleto) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO  PROMOCIONES_BOLETOS (id_peliculas,id_actor) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, promo_boleto.getBoleto().getCodigo());
            sentPrep.setInt(2, promo_boleto.getPromocion().getCodigo());
            
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(PromocionBoleto promo_boleto) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE PROMOCIONES_BOLETOS "
               + "id_pelicula = ?, "
               + "id_actor = ?, "
               + "WHERE id_promociones_boletos = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, promo_boleto.getBoleto().getCodigo());
            sentPrep.setInt(2, promo_boleto.getPromocion().getCodigo());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public PromocionBoleto get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM PROMOCIONES_BOLETOS WHERE id_promociones_boletos= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            PromocionBoleto promo_boleto = null;
            if (rs.next()) {
                int cod = rs.getInt("id_promociones_boletos");
                int peli = rs.getInt("id_pelicula");
                int actor = rs.getInt("id_actor");
                
                promo_boleto = new PromocionBoleto(cod,new BoletosDAO().get(peli),new PromocionesDAO().get(actor));
            }

            sentPrep.close();
            rs.close();

            return promo_boleto;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<PromocionBoleto> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<PromocionBoleto> listaPromocionBoletos = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM PROMOCIONES_BOLETOS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_promociones_boletos");
                int peli = rs.getInt("id_pelicula");
                int actor = rs.getInt("id_actor");

                listaPromocionBoletos.insertarAlFinal(new PromocionBoleto(cod,new BoletosDAO().get(peli),new PromocionesDAO().get(actor)));
            }

            rs.close();
            sentPrep.close();

            return listaPromocionBoletos;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM PROMOCIONES_BOLETOS WHERE id_promociones_boletos = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
