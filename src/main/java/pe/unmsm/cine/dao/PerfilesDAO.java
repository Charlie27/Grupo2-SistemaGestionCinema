/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Perfil;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class PerfilesDAO implements AbstractDAO<Perfil, Integer>{

    @Override
    public boolean registrar(Perfil perfil) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO PERFILES (perfil,descripcion) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, perfil.getPerfil());
            sentPrep.setString(2, perfil.getDescripcion());
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Perfil perfil) {
     try {

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;

            String sentencia = "UPDATE PERFILES "
                     + "SET perfil = ?,"
                    + "descripcion = ?, "
                    
                    + "WHERE id_perfil = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, perfil.getPerfil());
            sentPrep.setString(2, perfil.getDescripcion());
           

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }   

    @Override
    public Perfil get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM PERFILES WHERE id_perfil= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Perfil perfil = null;
            if (rs.next()) {
                int cod = rs.getInt("id_perfil");
                String tipo = rs.getString("perfil");
                String descripcion = rs.getString("descripcion");
                
                perfil = new Perfil(cod,tipo,descripcion);
            }

            sentPrep.close();
            rs.close();

            return perfil;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null; 
    }

    @Override
    public ListaDoble<Perfil> getAll() {
    try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Perfil> listaPerfiles = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM PERFILES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_perfil");
                String tipo = rs.getString("perfil");
                String descripcion = rs.getString("descripcion");
                
                listaPerfiles.insertarAlFinal(new Perfil(cod,tipo,descripcion));
            }

            rs.close();
            sentPrep.close();

            return listaPerfiles;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;    
    }

    @Override
    public boolean eliminar(Integer id) {
        try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM PERFILES WHERE id_perfil = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

}
