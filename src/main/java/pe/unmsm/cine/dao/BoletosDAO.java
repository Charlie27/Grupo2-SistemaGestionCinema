/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Boleto;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class BoletosDAO implements AbstractDAO<Boleto,Integer> {

    @Override
    public boolean registrar(Boleto boleto) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO BOLETOS (precio ,id_tipo_sala,id_tipo_boleto,id_dias) VALUES (?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1,boleto.getPrecio());
            sentPrep.setInt(2, boleto.getIdTipoSala());
            sentPrep.setInt(3, boleto.getIdTipoBoleto());
            sentPrep.setInt(4, boleto.getIdDia()); // no se si ponerle getCodigo y ahi vemos el codigo de la sala
                                                              // o llamar a getNombre y tener el nombre de la sala esa es mi duda.
                                                              //Si ven algo parecido es que tenia las mismas dudas en todas.

            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Boleto boleto) {
        /*try {
// (fila ,columna,ocupado,id_sala)

            Connection conn = AccesoDB.getConexion();
            PreparedStatement sentPrep = null;
            String sentencia = "UPDATE  BOLETOS"
                    + "SET fila = ?,"
                    + "columna = ?,"
                    + "ocupado= ?,"
                    + "id_sala= ?,"
                    + "WHERE id_asiento = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1,boleto.getPrecio());
            sentPrep.setInt(2, boleto.getIdTipoSala());
            sentPrep.setInt(3, boleto.getIdTipoBoleto());
            sentPrep.setInt(4, boleto.getDia().getCodigo());
            sentPrep.executeUpdate();

            conn.close();
            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }*/

        return false;
    }

    @Override
    public Boleto get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM BOLETOS WHERE id_boleto= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Boleto boleto = null;
            if (rs.next()) {
                
                
                int cod = rs.getInt("id_boleto");
                int precio = rs.getInt("precio");
                int tiposala = rs.getInt("id_tipo_sala");
                int tipoboleto = rs.getInt("id_tipo_boleto");
                int dia = rs.getInt("id_dias");  
                boleto = new Boleto(cod,precio,tiposala,tipoboleto,dia);
            }
            
            sentPrep.close();
            rs.close();

            return boleto;
         } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Boleto> getAll() {
        try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Boleto> listaBoletos = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM BOLETOS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_boleto");
                int precio = rs.getInt("precio");
                int tiposala = rs.getInt("id_tipo_sala");
                int tipoboleto = rs.getInt("id_tipo_boleto");
                int dia = rs.getInt("id_dias");  
                listaBoletos.insertarAlFinal(new Boleto(cod,precio,tiposala,tipoboleto,dia));
            }

            rs.close();
            sentPrep.close();

            return listaBoletos;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM BOLETOS WHERE id_boleto = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;   
    }
    
}
