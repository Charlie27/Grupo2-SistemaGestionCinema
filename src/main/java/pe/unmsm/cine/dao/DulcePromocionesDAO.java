
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.DulcePromocion;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class DulcePromocionesDAO implements AbstractDAO<DulcePromocion,Integer>{

    @Override
    public boolean registrar(DulcePromocion dulce_promo) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO DULCES_PROMOCIONES (id_dulce,id_promocion) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, dulce_promo.getDulce().getCodigo());
            sentPrep.setInt(2, dulce_promo.getPromocion().getCodigo());
            
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(DulcePromocion dulce_promo) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE DULCES_PROMOCIONES "
               + "id_dulce = ?, "
               + "id_promocion = ?, "
               + "WHERE id_dulces_promociones = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, dulce_promo.getDulce().getCodigo());
            sentPrep.setInt(2, dulce_promo.getPromocion().getCodigo());
            
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public DulcePromocion get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM DULCES_PROMOCIONES WHERE id_dulces_promociones= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            DulcePromocion dulce_promo = null;
            if (rs.next()) {
                int cod = rs.getInt("id_dulces_promociones");
                int dulce = rs.getInt("id_dulce");
                int promo = rs.getInt("id_promocion");
                
                dulce_promo = new DulcePromocion(cod,new DulcesDAO().get(dulce), new PromocionesDAO().get(promo));
            }

            sentPrep.close();
            rs.close();

            return dulce_promo;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<DulcePromocion> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<DulcePromocion> listaDulcesPromociones = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM DULCES_PROMOCIONES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_dulces_promociones");
                int dulce = rs.getInt("id_dulce");
                int promo = rs.getInt("id_promocion");
                
                listaDulcesPromociones.insertarAlFinal(new DulcePromocion(cod,new DulcesDAO().get(dulce), new PromocionesDAO().get(promo)));
            }

            rs.close();
            sentPrep.close();

            return listaDulcesPromociones;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM DULCES_PROMOCIONES WHERE id_dulces_promociones = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
