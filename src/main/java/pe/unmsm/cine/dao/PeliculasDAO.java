/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Clasificacion;
import pe.unmsm.cine.entidades.Estado;
import pe.unmsm.cine.entidades.Genero;
import pe.unmsm.cine.entidades.Idioma;
import pe.unmsm.cine.entidades.Pelicula;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class PeliculasDAO implements AbstractDAO<Pelicula,Integer>{
    @Override
    public boolean registrar(Pelicula pelicula) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO PELICULAS (nombre,duracion,director, img, img_slider,trailer,id_idioma,id_clasificacion,id_estado) VALUES (?,?,?,?,?,?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, pelicula.getNombre());
            sentPrep.setInt(2, pelicula.getDuracion());
            sentPrep.setString(3,pelicula.getDirector());
            sentPrep.setString(4, pelicula.getImagen());
            sentPrep.setString(5, pelicula.getImagen_slider());
            sentPrep.setString(4, pelicula.getTrailer());
            sentPrep.setInt(7, pelicula.getIdioma().getCodigo());
            sentPrep.setInt(8, pelicula.getClasificacion().getCodigo());
            sentPrep.setInt(9, pelicula.getEstado().getCodigo());

            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Pelicula pelicula) {
        try {

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;
//(nombres,apellido_paterno,apellido_materno,dni,password,id_perfil)
            String sentencia = "UPDATE PELICULAS "
                    + "SET nombre = ?,"
                    + "duracion = ?, "
                    + "director = ?, "
                    + "img = ?, "
                    + "img_slider = ?, "
                    + "trailer= ?, "
                    + "id_idiomar= ?, "
                    + "id_clasificacion= ?, "
                    + "id_estado= ?, "
                    + "WHERE id_pelicula = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, pelicula.getNombre());
            sentPrep.setInt(2, pelicula.getDuracion());
            sentPrep.setString(3,pelicula.getDirector());
            sentPrep.setString(4, pelicula.getImagen());
            sentPrep.setString(5, pelicula.getImagen_slider());
            sentPrep.setString(4, pelicula.getTrailer());
            sentPrep.setInt(7, pelicula.getIdioma().getCodigo());
            sentPrep.setInt(8, pelicula.getClasificacion().getCodigo());
            sentPrep.setInt(9, pelicula.getEstado().getCodigo());

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public Pelicula get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM PELICULAS WHERE id_pelicula= ? "
                    + "NATURAL JOIN IDIOMA "
                    + "NATURAL JOIN CLASIFICACION "
                    + "NATURAL JOIN ESTADOS ";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Pelicula pelicula = null;
            if (rs.next()) {
                int peli = rs.getInt("id_pelicula");
                String nombre = rs.getString("nombre");
                int duracion = rs.getInt("duracion");
                String director = rs.getString("director");
                String img= rs.getString("img");
                String img_slider = rs.getString("img_slider");
                String trailer = rs.getString("trailer");
                int idIdioma = rs.getInt("id_idioma");
                String idioma = rs.getString("idioma");
                int idClasif = rs.getInt("id_clasificacion");
                String clasificacion = rs.getString("clasificacion");
                int idEstado = rs.getInt("id_estado");
                String estado = rs.getString("estado");
                
                pelicula = new Pelicula(peli,nombre,duracion,director,img,img_slider,trailer,new Idioma(idIdioma, idioma),new Clasificacion(idClasif, clasificacion),new Estado(idEstado, estado));
            }

            sentPrep.close();
            rs.close();

            return pelicula;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Pelicula> getAll() {
        try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Pelicula> listaPeliculas = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT id_pelicula, nombre, duracion, director"
                    + ", img, img_slider, trailer, id_idioma, idioma, id_clasificacion"
                    + ", clasificacion, id_estado, estado FROM PELICULAS "
                    + "NATURAL JOIN IDIOMA "
                    + "NATURAL JOIN CLASIFICACION "
                    + "NATURAL JOIN ESTADOS ";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int peli = rs.getInt("id_pelicula");
                String nombre = rs.getString("nombre");
                int duracion = rs.getInt("duracion");
                String director = rs.getString("director");
                String img= rs.getString("img");
                String img_slider = rs.getString("img_slider");
                String trailer = rs.getString("trailer");
                int idIdioma = rs.getInt("id_idioma");
                String idioma = rs.getString("idioma");
                int idClasif = rs.getInt("id_clasificacion");
                String clasificacion = rs.getString("clasificacion");
                int idEstado = rs.getInt("id_estado");
                String estado = rs.getString("estado");
                
                listaPeliculas.insertarAlFinal(new Pelicula(peli,nombre,duracion,director,img,img_slider,trailer,new Idioma(idIdioma, idioma),new Clasificacion(idClasif, clasificacion),new Estado(idEstado, estado)));
            }

            rs.close();
            sentPrep.close();

            return listaPeliculas;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
        try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM PELICULAS WHERE id_pelicula = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }
}
