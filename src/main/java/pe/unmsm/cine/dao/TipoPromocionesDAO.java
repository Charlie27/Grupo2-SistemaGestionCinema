
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.TipoPromocion;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class TipoPromocionesDAO implements AbstractDAO<TipoPromocion,Integer>{

    @Override
    public boolean registrar(TipoPromocion tipo) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO TIPO_PROMOCION (tipo_promocion) VALUES (?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, tipo.getPromocion());
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(TipoPromocion tipo) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE TIPO_PROMOCION "
               + "tipo_promocion = ? "
               + "WHERE id_tipo = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, tipo.getPromocion());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public TipoPromocion get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM TIPO_PROMOCION WHERE id_tipo_promocion= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            TipoPromocion tipo = null;
            if (rs.next()) {
                int cod = rs.getInt("id_tipo");
                String promo = rs.getString("tipo_promocion");
                
                tipo = new TipoPromocion(cod,promo);
            }

            sentPrep.close();
            rs.close();

            return tipo;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<TipoPromocion> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<TipoPromocion> listaTipoPromocions = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM TIPO_PROMOCION";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_tipo_promocion");
                String promo = rs.getString("tipo_promocion");
                
                listaTipoPromocions.insertarAlFinal(new TipoPromocion(cod,promo));
            }

            rs.close();
            sentPrep.close();

            return listaTipoPromocions;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM TIPO_PROMOCION WHERE id_tipo_promocion = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
