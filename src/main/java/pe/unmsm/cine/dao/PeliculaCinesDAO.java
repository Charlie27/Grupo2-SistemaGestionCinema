
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.PeliculaCine;
import pe.unmsm.cine.estructuras.ListaDoble;
/**
 *
 * @author Jollja
 */
public class PeliculaCinesDAO implements AbstractDAO<PeliculaCine,Integer>{

    @Override
    public boolean registrar(PeliculaCine peli_actor) {
     try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO PELICULAS_CINES (id_peliculas,id_cine) VALUES (?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, peli_actor.getPelicula().getCodigo());
            sentPrep.setInt(2, peli_actor.getCine().getCodigo());
            
                        
            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(PeliculaCine peli_cine) {
        try {
       Connection conn = AccesoDB.getConnection();
       PreparedStatement sentPrep = null; 
       String sentencia = "UPDATE PELICULAS_CINES "
               + "id_pelicula = ?, "
               + "id_cine = ?, "
               + "WHERE id_peliculas_cines = ?;";
    
            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, peli_cine.getPelicula().getCodigo());
            sentPrep.setInt(2, peli_cine.getCine().getCodigo());
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public PeliculaCine get(Integer id) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM PELICULAS_CINES WHERE id_peliculas_cines= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            PeliculaCine peli_cine = null;
            if (rs.next()) {
                int cod = rs.getInt("id_peliculas_cine");
                int peli = rs.getInt("id_pelicula");
                int cine = rs.getInt("id_cine");
                
                peli_cine = new PeliculaCine(cod,new PeliculasDAO().get(peli),new CinesDAO().get(cine));
            }

            sentPrep.close();
            rs.close();

            return peli_cine;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<PeliculaCine> getAll() {
         try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<PeliculaCine> listaPeliculaCines = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM PELICULAS_CINES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_peliculas_cines");
                int peli = rs.getInt("id_pelicula");
                int cine = rs.getInt("id_cine");

                listaPeliculaCines.insertarAlFinal(new PeliculaCine(cod,new PeliculasDAO().get(peli),new CinesDAO().get(cine)));
            }

            rs.close();
            sentPrep.close();

            return listaPeliculaCines;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM PELICULAS_CINES WHERE id_peliculas_cines = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    } 
    
}
