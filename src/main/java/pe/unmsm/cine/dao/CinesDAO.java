/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Cine;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class CinesDAO implements AbstractDAO<Cine,Integer> {

    @Override
    public boolean registrar(Cine cine) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO CINES (nombre_cine , direccion,img ,nmr_salas,id_ciudad) VALUES (?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, cine.getNombre());
            sentPrep.setString(3, cine.getDireccion());
            sentPrep.setString(2, cine.getImg());
            sentPrep.setInt(4, cine.getSalas());
            sentPrep.setInt(5,cine.getIdCiudad());

            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Cine cine) {
        try {
// (nombre , direccion ,nmr_salas,id_distrito)

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;
            String sentencia = "UPDATE CINES "
                    + "SET nombre_cine = ?,"
                    + "direccion = ?, "
                    +"img = ?, "
                    + "nmr_salas=?,"
                    + "id_ciudad=?,"
                    + "WHERE id_cine = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, cine.getNombre());
            sentPrep.setString(2, cine.getDireccion());
            sentPrep.setString(3, cine.getImg());
            sentPrep.setInt(4, cine.getSalas());
            sentPrep.setInt(5, cine.getIdCiudad());

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public Cine get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM CINES WHERE id_cine= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Cine cine = null;
            if (rs.next()) {
                int cod = rs.getInt("id_cine");
                String nombre = rs.getString("nombre_cine");
                String direccion = rs.getString("direccion");
                String img=rs.getString("img");
                int nsalas = rs.getInt("nmr_salas");
                int idCiudad = rs.getInt("id_ciudad");  
                cine = new Cine(cod,nombre,direccion,img,nsalas,idCiudad);
            }

            sentPrep.close();
            rs.close();

            return cine;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Cine> getAll() {
        try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Cine> listaCines = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT id_cine, nombre_cine, direccion, img, nmr_salas, id_ciudad FROM CINES";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_cine");
                String nombre = rs.getString("nombre_cine");
                String direccion = rs.getString("direccion");
                String img=rs.getString("img");
                int nsalas = rs.getInt("nmr_salas");
                int idCiudad = rs.getInt("id_ciudad");  
                
                listaCines.insertarAlFinal(new Cine(cod,nombre,direccion,img,nsalas,idCiudad));
            }

            rs.close();
            sentPrep.close();

            return listaCines;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
    try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM CINES WHERE id_cine = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            conn.close();
            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }
    
    
}
