/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Sala;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author Jollja
 */
public class SalasDAO implements AbstractDAO<Sala,Integer> {

    @Override
    public boolean registrar(Sala sala) {
    try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO SALAS (nmr_sala , nmr_asientos ,aforo ,id_tipo_sala,id_cine) VALUES (?,?,?,?,?)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, sala.getNroSala());
            sentPrep.setInt(2, sala.getNroAsientos());
            sentPrep.setInt(3, sala.getCantMaxPersonas());
            sentPrep.setInt(4, sala.getTipo().getCodigo());
            sentPrep.setInt(5,sala.getCine().getCodigo());

            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Sala asiento) {
    try {
// (nmr_sala , nmr_asientos ,aforo ,id_tipo_sala,id_cine)

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;
            String sentencia = "UPDATE SALAS "
                    + "SET nmr_sala = ?,"
                    + "nmr_asientos = ?, "
                    + "aforo= ?,"
                    + "id_tipo_sala= ?,"
                    + "id_cine= ?,"
                    + "WHERE id_sala = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, asiento.getNroSala());
            sentPrep.setInt(2, asiento.getNroAsientos());
            sentPrep.setInt(3, asiento.getCantMaxPersonas());
            sentPrep.setInt(4, asiento.getTipo().getCodigo());
            sentPrep.setInt(5, asiento.getCine().getCodigo());
            
            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;    
    }

    @Override
    public Sala get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM SALAS WHERE id_sala= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Sala sala = null;
            if (rs.next()) {
                int cod = rs.getInt("id_sala");
                int n_sala = rs.getInt("nmr_sala");
                int n_asiento = rs.getInt("nmr_asiento");
                int n_maxpersonas = rs.getInt("aforo");
                int tipo_sala = rs.getInt("id_tipo_sala");
                int cine = rs.getInt("id_cine");
                sala = new Sala(cod,n_sala,n_asiento,n_maxpersonas,new TipoSalasDAO().get(tipo_sala),new CinesDAO().get(cine));
            }

            sentPrep.close();
            rs.close();

            return sala;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Sala> getAll() {
    try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Sala> listaSalas = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM SALAS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_sala");
                int n_sala = rs.getInt("nmr_sala");
                int n_asiento = rs.getInt("nmr_asiento");
                int n_maxpersonas = rs.getInt("aforo");
                int tipo_sala = rs.getInt("id_tipo_sala");
                int cine = rs.getInt("id_cine");
              listaSalas.insertarAlFinal(new Sala(cod,n_sala,n_asiento,n_maxpersonas,new TipoSalasDAO().get(tipo_sala),new CinesDAO().get(cine)));
            }

            rs.close();
            sentPrep.close();

            return listaSalas;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
         try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM SALAS WHERE id_sala = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }
    
}
