/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pe.unmsm.cine.entidades.Usuario;
import pe.unmsm.cine.estructuras.ListaDoble;
import pe.unmsm.cine.entidades.Perfil;
/**
 *
 * @author Jollja
 */
public class UsuarioDAO implements AbstractDAO<Usuario,Integer>{
    @Override
    public boolean registrar(Usuario usuario) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;
 
            String sentencia = "INSERT INTO USUARIOS VALUES (default,?,?,?,?,?,3)";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, usuario.getNombres());
            sentPrep.setString(2, usuario.getApellidoP());
            sentPrep.setString(3,usuario.getApellidoM());
            sentPrep.setString(4, usuario.getDni());
            sentPrep.setString(5, usuario.getContrasenia());

            sentPrep.executeUpdate();
            sentPrep.close();

        } catch (SQLException t) {
            System.out.println(t);
            return false;
        }
        return true;
    }

    @Override
    public boolean modificar(Usuario usuario) {
        try {

            Connection conn = AccesoDB.getConnection();
            PreparedStatement sentPrep = null;
            
            String sentencia = "UPDATE USUARIOS "
                    + "SET nombres = ?,"
                    + "apellido_paterno = ?, "
                    + "apellido_materno = ?, "
                    + "dni = ?, "
                    + "password = ?, "
                    + "id_perfil = ? "
                    + "WHERE id_usuario = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, usuario.getNombres());
            sentPrep.setString(2, usuario.getApellidoP());
            sentPrep.setString(3, usuario.getApellidoM());            
            sentPrep.setString(4, usuario.getDni());
            sentPrep.setString(5, usuario.getContrasenia());
            sentPrep.setInt(6, usuario.getIdPerfil());

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;

        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }

    @Override
    public Usuario get(Integer id) {
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM USUARIOS WHERE id_usuario= ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            rs = sentPrep.executeQuery();

            Usuario usuario = null;
            if (rs.next()) {
                int cod = rs.getInt("id_usuario");
                String nombres = rs.getString("nombres");
                String apellidom = rs.getString("apellido_paterno");
                String apellidop = rs.getString("apellido_materno");
                String dni = rs.getString("dni");
                String contraseña = rs.getString("password");
                int idPerfil = rs.getInt("id_perfil");
                
                usuario = new Usuario(cod,nombres,apellidop,apellidom,dni,contraseña,idPerfil);
            }

            sentPrep.close();
            rs.close();

            return usuario;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public ListaDoble<Usuario> getAll() {
        try {

            ResultSet rs = null;
            Connection conn = null;
            PreparedStatement sentPrep = null;
            ListaDoble<Usuario> listaUsuarios = new ListaDoble<>();

            conn = AccesoDB.getConnection();

            String sentencia = "SELECT * FROM USUARIOS";
            sentPrep = conn.prepareStatement(sentencia);

            rs = sentPrep.executeQuery();

            while (rs.next()) {
                int cod = rs.getInt("id_usuario");
                String nombres = rs.getString("nombres");
                String apellidop = rs.getString("apellido_paterno");
                String apellidom = rs.getString("apellido_materno");
                String dni = rs.getString("dni");
                String contraseña = rs.getString("password");
                int idPerfil = rs.getInt("id_perfil");
                
                listaUsuarios.insertarAlFinal(new Usuario(cod,nombres,apellidop,apellidom,dni,contraseña,idPerfil));
            }

            rs.close();
            sentPrep.close();

            return listaUsuarios;
        } catch (SQLException e) {
            System.out.println(e);
        }

        /* Si no encontro ningun dato, retornamos null */
        return null;
    }

    @Override
    public boolean eliminar(Integer id) {
        try {

            Connection conn = AccesoDB.getConnection();;

            PreparedStatement sentPrep = null;

            String sentencia = "DELETE FROM USUARIOS WHERE id_usuario = ?;";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setInt(1, id);

            sentPrep.executeUpdate();

            sentPrep.close();

            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return false;
    }
    
    public Usuario getByDni(String user){
        try {
            Connection conn = AccesoDB.getConnection();

            PreparedStatement sentPrep = null;

            ResultSet rs = null;

            String sentencia = "SELECT * FROM USUARIOS WHERE dni = ?";

            sentPrep = conn.prepareStatement(sentencia);
            sentPrep.setString(1, user);

            rs = sentPrep.executeQuery();

            Usuario usuario = null;
            if (rs.next()) {
                int cod = rs.getInt("id_usuario");
                String nombres = rs.getString("nombres");
                String apellidom = rs.getString("apellido_paterno");
                String apellidop = rs.getString("apellido_materno");
                String dni = rs.getString("dni");
                String contraseña = rs.getString("password");
                int idPerfil = rs.getInt("id_perfil");
                
                usuario = new Usuario(cod,nombres,apellidop,apellidom,dni,contraseña,idPerfil);
            }

            sentPrep.close();
            rs.close();

            return usuario;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
}
