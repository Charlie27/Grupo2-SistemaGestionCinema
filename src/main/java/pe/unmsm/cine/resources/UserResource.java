/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.unmsm.cine.resources;

import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.mindrot.jbcrypt.BCrypt;
import pe.unmsm.cine.dao.EmpleadoDAO;
import pe.unmsm.cine.dao.UsuarioDAO;
import pe.unmsm.cine.entidades.Empleado;
import pe.unmsm.cine.entidades.ErrorMessage;
import pe.unmsm.cine.entidades.Usuario;
import pe.unmsm.cine.estructuras.ListaDoble;

/**
 *
 * @author charlie
 */
@Path("/users")
@Produces("application/json")
public class UserResource {

    private UsuarioDAO usuarioDAO;
    private EmpleadoDAO empleadoDAO;

    @GET
    @Path("")
    public Response getUsers(){
        ListaDoble<Usuario> usuarios;
        Object res = new ErrorMessage("Error en el servidor");
        Status status = Status.INTERNAL_SERVER_ERROR;
        usuarioDAO = new UsuarioDAO();
        usuarios = usuarioDAO.getAll();
        if(usuarios != null) {
            res = usuarios.toArray();
            status = Status.OK;
        }
        return Response.status(status).entity(res).build();
    }
    
    @POST
    @Path("")
    public Response register(@FormParam("name") String name, @FormParam("apePat") String apePat, @FormParam("apeMat") String apeMat,
            @FormParam("dni") String dni, @FormParam("password") String password, @FormParam("photo") String photo, @FormParam("cine") int cine){
        Object o = new ErrorMessage("Error en el servidor");
        Status status = Status.INTERNAL_SERVER_ERROR;
        System.out.println("peticion de registro");
        password = BCrypt.hashpw(password, BCrypt.gensalt());
        usuarioDAO = new UsuarioDAO();
        if(usuarioDAO.getByDni(dni) == null){
            if(photo != null){
                empleadoDAO = new EmpleadoDAO();
                o = empleadoDAO.registrar(name, apePat, apeMat, dni, password, photo, cine, 2);
                status = Status.OK;
            }else{
                if(usuarioDAO.registrar(new Usuario(name, apeMat, apeMat, dni, password, 3))){
                    o = usuarioDAO.getByDni(dni);
                    status = Status.OK;
                }
            }
        }else{
            o = new ErrorMessage("Ya existe un usuario registrado con el dni");
        }
        return Response.status(status).entity(o)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Headers","origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
    }
    
    @POST
    @Path("/login")
    public Response doDoLoginUser(@FormParam("password") String password, @FormParam("user") String user) {
        Usuario usuario = null;
        Empleado empleado  = null;
        Object res ;
        Status status = Status.OK;
        if(user != null && password != null){
            
            usuarioDAO = new UsuarioDAO();
            usuario = usuarioDAO.getByDni(user);

            if(usuario != null){
                if(BCrypt.checkpw(password, usuario.getContrasenia())){
                    if(usuario.getIdPerfil() == 2 || usuario.getIdPerfil() == 1){
                        empleadoDAO = new EmpleadoDAO();
                        res = empleadoDAO.getByUserId(usuario.getCodigo());
                    }else{
                        res = usuario;
                    }
                }else{
                    status = Status.INTERNAL_SERVER_ERROR;
                    res = new ErrorMessage("La contraseña es incorrecta");
                }
            }else{
                status = Status.INTERNAL_SERVER_ERROR;
                res = new ErrorMessage("No existe el usuario");
            }
        }else{
            status = Status.INTERNAL_SERVER_ERROR;
            res = new ErrorMessage("falta enviar datos");
        }
        return Response.status(status).entity(res).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Headers","origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS, HEAD").build();
    }
}
